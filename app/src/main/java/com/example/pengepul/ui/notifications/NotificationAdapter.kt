package com.example.pengepul.ui.notifications

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pengepul.R
import com.example.pengepul.model.NotificationModel
import com.example.pengepul.utils.Constants
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_notifications.view.*
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter(val options: FirestoreRecyclerOptions<NotificationModel>) :
    FirestoreRecyclerAdapter<NotificationModel, NotificationAdapter.NotificationViewHolder>(options!!) {
//    private val list_notif = mutableListOf<NotificationModel>()

    private var listener: OnClickListenerCallback? = null
    fun setOnClickListenerCallback(listener: OnClickListenerCallback) {
        this.listener = listener
    }

    override fun updateOptions(options: FirestoreRecyclerOptions<NotificationModel>) {
        super.updateOptions(options)
    }

    fun deleteData(position: Int) {
        snapshots.getSnapshot(position).reference.delete()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationAdapter.NotificationViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_notifications, parent, false)
        return NotificationViewHolder(view)
    }

    inner class NotificationViewHolder(private val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(notification: NotificationModel, position: Int) {
            fun Date?.parseDate(): String {
                val inputFormat = notification.date
                val date = inputFormat?.toDate()
                val outputFormat = java.text.SimpleDateFormat("dd/MM/YYYY", Locale("ID"))
                return outputFormat.format(date)
            }
            if (notification.data.get(Constants.TYPE) == Constants.SHOP) {
                v.ll_optionOrder.visibility = View.VISIBLE
            } else {
                v.ll_optionOrder.visibility = View.GONE
            }
            v.tv_judulNotification.text = notification.data.get("title")
            v.tv_namaUserNotif.text = notification.data.get("nama_user")
            v.tv_typeNotif.text = notification.data.get("type")
            v.tv_tanggalNotif.text = notification.date?.toDate().parseDate()

            listener.let {
                if (notification.data.get(Constants.TYPE) == Constants.SHOP) {
                    v.btn_accept.setOnClickListener {
                        listener?.onClickAccept(notification, position)
                    }
                    v.btn_reject.setOnClickListener {
                        listener?.onClickReject(notification, position)
                    }
                } else if (notification.data.get(Constants.TYPE) == Constants.CHAT) {
                    v.setOnClickListener {
                        listener?.onClickListenerChat(notification, position)
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(
        holder: NotificationAdapter.NotificationViewHolder,
        position: Int,
        model: NotificationModel
    ) {
        holder.bind(model, position)
    }

//    inner class NotificationViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
//        fun bind(notification: NotificationModel, position: Int) {
//            if (notification.data.get("type") == "Shop") {
//                view.ll_optionOrder.visibility = View.VISIBLE
//            } else {
//                view.ll_optionOrder.visibility = View.GONE
//            }
//
//            fun Date?.parseDate(): String{
//                val inputFormat = notification.date
//                val date = inputFormat?.toDate()
//                val outputFormat = SimpleDateFormat("dd/MM/YYYY", Locale("ID"))
//                return outputFormat.format(date)
//            }
//
//            view.tv_judulNotification.text = notification.data.get("title")
//            view.tv_namaUserNotif.text = notification.data.get("nama_user")
//            view.tv_typeNotif.text = notification.data.get("type")
//            view.tv_tanggalNotif.text = notification.date?.toDate().parseDate()
//
//            listener.let {
//                if (notification.data.get(Constants.TYPE) == Constants.SHOP) {
//
//                    view.btn_accept.setOnClickListener {
//                        listener.onClickAccept(notification, position)
//                    }
//                    view.btn_reject.setOnClickListener {
//                        listener.onClickReject(notification, position)
//                    }
//                } else if (notification.data.get(Constants.TYPE) == Constants.CHAT) {
//                    view.setOnClickListener {
//                        listener.onClickListenerChat(notification, position)
//                    }
//                }
//            }
//        }
//    }

//    fun setData(listNotif: List<NotificationModel>) {
//        this.list_notif.clear()
//        this.list_notif.addAll(listNotif)
//        notifyDataSetChanged()
//    }

//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
//        val view =
//            LayoutInflater.from(parent.context).inflate(R.layout.item_notifications, parent, false)
//        return NotificationViewHolder(view)
//    }

    interface OnClickListenerCallback {
        fun onClickAccept(notification: NotificationModel, position: Int)
        fun onClickReject(notification: NotificationModel, position: Int)
        fun onClickListenerChat(notification: NotificationModel, posistion: Int)
    }

//    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
//        holder.bind(list_notif[position],position)
//    }

//    override fun getItemCount(): Int {
//        return list_notif.size
//    }
}