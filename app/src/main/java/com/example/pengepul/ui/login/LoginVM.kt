package com.example.pengepul.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.firebase.FirebaseAuthRepo
import com.example.pengepul.utils.DispatcherProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class LoginVM(private val repoAuth: FirebaseAuthRepo, private val dispatcher: DispatcherProvider) :
    ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    fun login(email: String, password: String){
        CoroutineScope(dispatcher.io()).launch {
            try {
                when(val result = repoAuth.login(email, password)){
                    is RepositoryResult.Success ->{
                        withContext(dispatcher.main()){
                            _successLiveData.value = "Berhasil Login"
                        }
                    }
                    is RepositoryResult.Error -> withContext(dispatcher.main()){
                        _errorLiveData.value = "Email atau password Tidak Terdaftar"
                    }
                    is RepositoryResult.Canceled -> withContext(dispatcher.main()){
                        _errorLiveData.value = result.exception?.message
                    }
                }
            }catch (e: Exception){
                withContext(dispatcher.main()){
                    _errorLiveData.value = e.message
                }
            }
        }
    }

    class Factory(
        private val repoAuth: FirebaseAuthRepo,
        private val dispatcher: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginVM(repoAuth, dispatcher) as T
        }
    }
}