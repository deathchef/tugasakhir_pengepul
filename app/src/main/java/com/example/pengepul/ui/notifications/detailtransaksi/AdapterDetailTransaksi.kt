package com.example.pengepul.ui.notifications.detailtransaksi

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pengepul.R
import com.example.pengepul.ui.mainmenu.transaksi.RongsokTransaksiAdapter
import kotlinx.android.synthetic.main.item_detail_transaksi.view.*
import java.util.ArrayList

class AdapterDetailTransaksi(
    val context: Context,
    var list_rongsok: ArrayList<RongsokTransaksiAdapter>
) : RecyclerView.Adapter<AdapterDetailTransaksi.listRongsokTransaksiHolder>() {
    inner class listRongsokTransaksiHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(rongsok: RongsokTransaksiAdapter) {
            itemView.tv_namaRongsokTransaksi.text = rongsok.nama_rongsok
            itemView.tv_berat_rongsokTransaksi.text = rongsok.quantity.toString()
            itemView.tv_harga_rongsok_transaksi.text = rongsok.harga_rongsok.toString()
            Glide.with(itemView.context)
                .load(rongsok.foto_rongsokUri)
                .into(itemView.iv_fotoDetailTransaksi)
        }
    }

    fun setData(list_rongsok: List<RongsokTransaksiAdapter>) {
        this.list_rongsok.clear()
        this.list_rongsok.addAll(list_rongsok)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDetailTransaksi.listRongsokTransaksiHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_detail_transaksi, parent, false)
        return listRongsokTransaksiHolder(view)
    }

    override fun onBindViewHolder(
        holder: AdapterDetailTransaksi.listRongsokTransaksiHolder,
        position: Int
    ) {
        holder.bind(list_rongsok[position])
    }

    override fun getItemCount(): Int {
        return list_rongsok.size
    }
}