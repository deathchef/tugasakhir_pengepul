package com.example.pengepul.ui.mainmenu.rongsok

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pengepul.R
import com.example.pengepul.model.Rongsok
import com.example.pengepul.model.RongsokAdapter
import kotlinx.android.synthetic.main.item_rongsok.view.*

class AdapterRongsok() :
    RecyclerView.Adapter<AdapterRongsok.listRongsokHolder>() {
    private val listRongsok = mutableListOf<RongsokAdapter>()
    private lateinit var listener: OnClickListenerCallback

    fun setOnClickListener(listenerCallback: OnClickListenerCallback){
        this.listener = listenerCallback
    }

    inner class listRongsokHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(rongsok: RongsokAdapter) {
            view.tv_namaRongsok.text = rongsok.nama_rongsok
            view.tv_hargaRongsok.text = rongsok.harga_rongsok.toString()
            Glide.with(view.context)
                .load(rongsok.foto_rongsokUri)
                .into(view.iv_photoRongsok)

            Log.d("Ready", "Result : ${rongsok.isReady}")
            view.btn_ubah_status_rongsok.isChecked = rongsok.isReady

            listener.let {
                itemView.setOnClickListener {
                    listener.onClickListenerCallback(rongsok, layoutPosition)
                }
                view.btn_ubah_status_rongsok.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked){
                        listener.readyItem(rongsok,layoutPosition)
                    }else {
                        listener.unReadyItem(rongsok, layoutPosition)
                    }
                }
            }
        }
    }

    fun setData(listRongsok: List<RongsokAdapter>) {
        this.listRongsok.clear()
        this.listRongsok.addAll(listRongsok)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): listRongsokHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_rongsok, parent, false)
        return listRongsokHolder(view)
    }

    override fun onBindViewHolder(holder: listRongsokHolder, position: Int) {
        holder.bind(listRongsok[position])
    }

    override fun getItemCount(): Int {
        return listRongsok.size
    }

    interface OnClickListenerCallback{
        fun onClickListenerCallback(rongsok: RongsokAdapter, position: Int)
        fun readyItem(rongsok: RongsokAdapter, position: Int)
        fun unReadyItem(rongsok: RongsokAdapter, position: Int)
    }
}