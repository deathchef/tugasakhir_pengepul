package com.example.pengepul.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.pengepul.R
import com.example.pengepul.ui.mainmenu.transaksi.ModelTransaksi
import com.example.pengepul.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.chat_row_from.view.*
import kotlinx.android.synthetic.main.chat_row_to.view.*

const val TAG = "LogChatPengepul"

class ActivityChat : AppCompatActivity() {
    var auth = FirebaseAuth.getInstance()
    val adapter = GroupAdapter<ViewHolder>()
    private val dataIntent by lazy {
        intent.getParcelableExtra<ModelTransaksi>("dataChat")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        container_chat_pengepul.adapter = adapter
        listenMessage()

        tv_nama_pelanggan.text = dataIntent.nama_pelanggan

        btn_send_chat.setOnClickListener {
            sendMessages()
            Log.d(TAG, "Mengirim Pesan")
            et_chat.text.clear()
        }
        Log.d("data intent","result: $dataIntent")
    }

    private fun sendMessages() {
        val text = et_chat.text.toString()

//        val fromId = auth.currentUser?.uid!!  //id pelanggan
        val fromId = dataIntent.id_pengepul
        val toId= dataIntent.id_user //idpengepul
        Log.d("id","id Pengepul ${dataIntent.id_user}")

        if(fromId == null) return

        val reference = FirebaseDatabase.getInstance().getReference("/${Constants.CHAT}").push()

        val chatMsg = Chat(reference.key!!,text, fromId, toId, System.currentTimeMillis()/1000)
        reference.setValue(chatMsg)
            .addOnSuccessListener {
                Log.d(TAG,"Simpan Pesan Chat: ${reference.key}")
            }
    }

    private fun listenMessage() {
        val ref = FirebaseDatabase.getInstance().getReference("/${Constants.CHAT}")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val chatMessage = snapshot.getValue(Chat::class.java)
                if (chatMessage != null) {
                    Log.d(TAG, chatMessage.text)
                    if (chatMessage.fromId == dataIntent.id_pengepul){
                        adapter.add(ChatToItem(chatMessage.text))
                    }else{
                        adapter.add(ChatFromItem(chatMessage.text))
                    }
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }
}

class ChatFromItem(val text: String): Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.tv_chat_from.text = text
    }
    override fun getLayout(): Int {
        return  R.layout.chat_row_from
    }
}
class ChatToItem(val text: String): Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.tv_chat_to.text = text
    }
    override fun getLayout(): Int {
        return R.layout.chat_row_to
    }
}