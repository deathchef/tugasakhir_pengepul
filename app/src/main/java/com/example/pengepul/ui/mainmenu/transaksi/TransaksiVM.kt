package com.example.pengepul.ui.mainmenu.transaksi

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.firebase.FirebaseTransaksiRepo
import com.example.pengepul.utils.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class TransaksiVM(
    private val auth: FirebaseAuth,
    private val repoTransaksi: FirebaseTransaksiRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _listDataTransaksi = MutableLiveData<MutableList<ModelTransaksi>>()
    val listDataTransaksi: LiveData<MutableList<ModelTransaksi>> get() = _listDataTransaksi

    fun getListTransaksi() {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result =
                    repoTransaksi.getDataTransaksi(auth.currentUser?.uid.toString())) {
                    is RepositoryResult.Success -> {
                        _listDataTransaksi.postValue(result.data)
                    }
                    is RepositoryResult.Error ->
                        withContext(dispatchers.main()) {
                            _errorLiveData.postValue("Gagal Memuat Data Transaksi")
                        }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoTransaksi: FirebaseTransaksiRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return TransaksiVM(auth, repoTransaksi, dispatchers) as T
        }
    }
}