package com.example.pengepul.ui.mainmenu.toko

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.pengepul.R
import com.example.pengepul.model.Pengepul
import com.example.pengepul.ui.maps.MapsActivity
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.yalantis.ucrop.UCrop
import com.yalantis.ucrop.UCropActivity
import com.yalantis.ucrop.view.UCropView
import kotlinx.android.synthetic.main.dialog_edit_profil.view.*
import kotlinx.android.synthetic.main.dialog_edittoko.view.*
import kotlinx.android.synthetic.main.dialog_tambahtoko.*
import kotlinx.android.synthetic.main.dialog_tambahtoko.view.*
import kotlinx.android.synthetic.main.fragment_toko.*
import org.koin.android.ext.android.inject
import java.util.*

const val ACTIVITY_MAPS = 2
const val ACTIVITY_MAPS_NEW = 3

class TokoFragment : Fragment() {
    private lateinit var showDialog: AlertDialog

    private val dialogTambahToko by lazy {
        layoutInflater.inflate(R.layout.dialog_tambahtoko, null, false)
    }
    private val dialog_edit_toko by lazy {
        layoutInflater.inflate(R.layout.dialog_edittoko, null, false)
    }

    private lateinit var toko: Toko
    private lateinit var viewModel: TokoVM

    private val factory by inject<TokoVM.Factory>()

    private var fileUri: Uri? = null
    private lateinit var uriPhoto: Uri

    private var addressToko = ""
    private var latitude = 0.0
    private var longitude = 0.0

    var activity_dialog = false
    var activity_dialog_edit = false

    var tokenFcm = ""
    var teleponToko = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_toko, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this, factory).get(TokoVM::class.java)
        viewModel.successLiveData.observe(viewLifecycleOwner, onSuccess())
        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.dataTokoLiveData.observe(viewLifecycleOwner, observeGetDataToko())
        viewModel.photoLiveData.observe(viewLifecycleOwner, onSuccessDownloadPhoto())
        viewModel.dataPengepul.observe(viewLifecycleOwner, observeGetDataPengepul())

        showLoading(true)
        viewModel.getDataToko()
        viewModel.getPengepul()

        btn_addToko.setOnClickListener {
            this.let {
                activity_dialog = true
                if (dialogTambahToko.parent != null) {
                    //deklarasi kalau dialog tambah toko menjadi viewgroup agar dapat ditampilkan
                    val viewGroup = dialogTambahToko.parent as ViewGroup
                    viewGroup.removeView(dialogTambahToko)
                }
                val dialog = AlertDialog.Builder(requireContext())
                dialog.setView(dialogTambahToko)
                showDialog = dialog.create()
                showDialog.show()

                // mengecek data yang ditambah apakah kosong atau telah memiliki isi
                dialogTambahToko.btn_tambahToko.setOnClickListener {

                    if (dialogTambahToko.et_namaToko?.text.toString().isNullOrEmpty()) {
                        dialogTambahToko.et_namaToko?.error = "Nama Toko Harus diisi"
                        dialogTambahToko.et_namaToko?.isFocusable = true
                    } else if (
                        dialogTambahToko.tv_tambahDetailAlamat.text.toString().isNullOrEmpty()) {
                        Toast.makeText(
                            dialogTambahToko.context,
                            "Data Alamat Kosong",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    } else if (dialogTambahToko.iv_photoToko == null) {
                        Toast.makeText(
                            dialogTambahToko.context,
                            "Tidak Ada Foto",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {
                        showLoading(true)
                        observeAddToko()
                        dialogTambahToko.tv_tambahDetailAlamat.text = ""
                        viewModel.uploadPhoto(
                            dialogTambahToko.et_namaToko.text.toString(),
                            fileUri!!
                        )
                        viewModel.getDataToko()
                        showDialog.dismiss()
                    }
                }

                //menambahkan gambar untuk toko
                dialogTambahToko.btn_addPhoto.setOnClickListener {
                    CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(requireActivity(), this@TokoFragment)
                }

                dialogTambahToko.btn_lokasiToko.setOnClickListener {
                    val intent = Intent(requireContext(), MapsActivity::class.java)
                    startActivityForResult(intent, ACTIVITY_MAPS)
                }
            }
        }

        btn_editToko.setOnClickListener {
            this.let {
                activity_dialog_edit = true
                if (dialog_edit_toko.parent != null) {
                    //deklarasi kalau dialog tambah toko menjadi viewgroup agar dapat ditampilkan
                    val viewGroup = dialog_edit_toko.parent as ViewGroup
                    viewGroup.removeView(dialog_edit_toko)
                }
                val dialog = AlertDialog.Builder(requireContext())
                dialog.setView(dialog_edit_toko)
                showDialog = dialog.create()
                showDialog.show()

                // mengecek data yang ditambah apakah kosong atau telah memiliki isi
                dialog_edit_toko.btn_simpan_edit_toko.setOnClickListener {

                    if (dialog_edit_toko.et_edit_nama_toko?.text.toString().isNullOrEmpty()) {
                        Toast.makeText(
                            dialog_edit_toko.context,
                            "Nama Toko Tidak Boleh Kosong",
                            Toast.LENGTH_LONG
                        ).show()
                        dialog_edit_toko.et_edit_nama_toko?.error = "Nama Toko Harus diisi"
                        dialog_edit_toko.et_edit_nama_toko?.isFocusable = true
                    } else if (
                        dialog_edit_toko.tv_edit_alamat_toko?.text.toString().isNullOrEmpty()) {
                        Toast.makeText(
                            dialog_edit_toko.context,
                            "Data Alamat Baru Kosong",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    } else if (fileUri!! == null) {
                        Toast.makeText(
                            dialog_edit_toko.context,
                            "Tidak Ada Foto",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    } else {
                        showLoading(true)
                        observeEdtiToko()
                        dialog_edit_toko.tv_edit_alamat_toko.text = ""
                        viewModel.uploadPhoto(
                            dialog_edit_toko.et_edit_nama_toko.text.toString(),
                            fileUri!!
                        )
                        viewModel.getDataToko()
                        Toast.makeText(requireContext(),"Berhasil Ubah Data Toko", Toast.LENGTH_LONG).show()
                        showDialog.dismiss()
                    }
                }

                //menambahkan gambar untuk toko
                dialog_edit_toko.btn_ubah_photo.setOnClickListener {
                    CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(requireActivity(), this@TokoFragment)
                }

                dialog_edit_toko.btn_lokasi_baru_toko.setOnClickListener {
                    val intentEdit = Intent(requireContext(), MapsActivity::class.java)
                    startActivityForResult(intentEdit, ACTIVITY_MAPS)
                }

                dialog_edit_toko.btn_batal_edit_toko.setOnClickListener {
                    showDialog.cancel()
                }
            }
        }
    }

    private fun observeGetDataPengepul() = Observer<Pengepul> {
        activity?.runOnUiThread {
            tokenFcm = it.tokenFcm
            teleponToko = it.noHp
        }
    }

    private fun onSuccessDownloadPhoto() = Observer<Uri> { uri ->
        this.uriPhoto = uri
        Glide.with(requireActivity())
            .load(uri)
            .apply(RequestOptions().centerCrop())
            .into(iv_toko)

    }

    fun observeEdtiToko(){
        val nama_toko = dialog_edit_toko.et_edit_nama_toko?.text.toString()
        viewModel.editToko(nama_toko, latitude, longitude, addressToko)
    }

    fun observeAddToko() {
        val nama = dialogTambahToko.et_namaToko?.text.toString()
//        val alamat = dialogTambahToko.et_dialog_alamatToko?.text.toString()
        viewModel.addToko(nama, latitude, longitude, addressToko, tokenFcm, teleponToko)
    }

    fun observeGetDataToko() = Observer<Toko> {
        activity?.runOnUiThread {
            this.toko = it
            Log.d("Data Toko", "Toko : $it")
            if (it.id_pengepul == "") {
                cardView_toko.visibility = View.INVISIBLE
                btn_addToko.visibility = View.VISIBLE
                tv_noToko.visibility = View.VISIBLE
                btn_buka_toko.visibility = View.INVISIBLE
                showLoading(false)
                Toast.makeText(requireContext(), "Belum memiliki Toko", Toast.LENGTH_SHORT).show()

            } else {
                if (it.rating == 0.0){
                    tv_ratingToko.text = it.rating.toString()
                }else{
                    var rataRating = it.rating / it.pengunjung
                    tv_ratingToko.text = rataRating.toString().subSequence(0,3)
                }
                tv_namaToko.text = it.nama_toko
                tv_alamatToko.text = it.address
                cardView_toko.visibility = View.VISIBLE
                btn_buka_toko.visibility = View.VISIBLE
                btn_addToko.visibility = View.GONE
                tv_noToko.visibility = View.GONE
                viewModel.downloadPhoto(it.nama_toko)
                showLoading(false)

                if (it.isOpen == false){
                    btn_buka_toko.setText("Buka Toko")
                    btn_buka_toko.setOnClickListener {
                        viewModel.openTokoOperation()
                        btn_buka_toko.setText("Tutup Toko")
                    }
                }else if (it.isOpen == true){
                    btn_buka_toko.setText("Tutup Toko")
                    btn_buka_toko.setOnClickListener {
                        viewModel.closeTokoOperation()
                        btn_buka_toko.setText("Buka Toko")
                    }
                }
            }
        }
    }

    private fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
    }

    private fun onSuccess() = Observer<String> {
        showDialog.dismiss()
        Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                Log.d(
                    "Cek Boolean Act",
                    "result: act_dialog ($activity_dialog), act_dialog_edit ($activity_dialog_edit)"
                )
                if (activity_dialog == true) {
                    fileUri = result.uri
                    Log.d("Gambar Toko: ", fileUri?.path.toString())
                    Glide.with(this@TokoFragment).load(fileUri).into(dialogTambahToko.iv_photoToko)
                } else if (activity_dialog_edit == true) {
                    fileUri = result.uri
                    Log.d("Edit Gambar Toko", "result: ${fileUri?.path}")
                    Glide.with(this@TokoFragment).load(fileUri)
                        .into(dialog_edit_toko.iv_edit_photoToko)
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e("Pict Toko: ", error.toString())
            }
        } else if (requestCode == ACTIVITY_MAPS) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(
                    "Cek Boolean Act",
                    "result: act_dialog ($activity_dialog), act_dialog_edit ($activity_dialog_edit)"
                )
                if (activity_dialog == true){
                    latitude = data!!.getDoubleExtra("latitude", 0.0)
                    longitude = data.getDoubleExtra("longitude", 0.0)
                    addressToko = data.getStringExtra("address")!!

                    dialogTambahToko.tv_tambahDetailAlamat.text = addressToko
                    Log.d("Edit Address","result: $addressToko")

                }else if (activity_dialog_edit == true){
                    latitude = data!!.getDoubleExtra("latitude", 0.0)
                    longitude = data.getDoubleExtra("longitude", 0.0)
                    addressToko = data.getStringExtra("address")!!

                    dialog_edit_toko.tv_edit_alamat_toko.text = addressToko
                    Log.d("Edit Address","result: $addressToko")
                }
            } else {
                Toast.makeText(
                    this@TokoFragment.requireContext(),
                    "Data Intent Gagal",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }


    fun showLoading(state: Boolean) {
        if (state) {
            pb_toko.visibility = View.VISIBLE
        } else {
            pb_toko.visibility = View.GONE
        }
    }
}