package com.example.pengepul.ui.login

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.example.pengepul.R
import com.example.pengepul.model.Pengepul
import com.example.pengepul.ui.mainmenu.MainMenu
import com.example.pengepul.ui.register.RegisActivity
import com.google.android.material.snackbar.Snackbar
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_regis.*
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {
    private val LOGIN = LoginActivity::class.java.simpleName
    private val dialog: AlertDialog by lazy {
        SpotsDialog.Builder().setContext(this).build()
    }
    private lateinit var viewModel: LoginVM
    private val factory: LoginVM.Factory by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        viewModel = ViewModelProvider(this, factory).get(LoginVM::class.java)
        viewModel.errorLiveData.observe(this, onError())
        viewModel.successLiveData.observe(this, onSuccess())

        btn_login.setOnClickListener {
            showLoading(true)
            loginUser()
        }

        tv_toRegis.setOnClickListener {
            startActivity(Intent(this, RegisActivity::class.java))
        }
    }

    private fun loginUser() {
        val email = et_login.text.toString().trim { it <= ' ' }
        val password = et_passwordLogin.text.toString().trim { it <= ' ' }

        when {
            email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                showError("Email Tidak Valid")
                showLoading(false)
            }
            password.isNullOrEmpty() || password.length < 8 -> {
                showError("Password Tidak Boleh Kosong atau Kurang Dari 8 Karakter")
                showLoading(false)
            }
            else -> {
                viewModel.login(email, password)
                Log.d(LOGIN, "Info Login : Email($email)")
            }
        }
        showLoading(false)
    }


    fun onSuccess() = Observer<String> {
        showLoading(false)
        runOnUiThread {
            startActivity(Intent(this, MainMenu::class.java)).apply {
                Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    fun onError() = Observer<String> {
        runOnUiThread {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            showLoading(false)
        }
    }

    fun showError(msg: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.errorMsg))
        snackbar.setTextColor(ContextCompat.getColor(this, R.color.teksHitam))
        snackbar.show()
    }

    fun showLoading(state:Boolean){
        if (state){
            pb_login.visibility = View.VISIBLE
        }else{
            pb_login.visibility = View.GONE
        }
    }
}