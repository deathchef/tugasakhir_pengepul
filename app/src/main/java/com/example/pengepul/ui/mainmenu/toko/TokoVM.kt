package com.example.pengepul.ui.mainmenu.toko

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.Pengepul
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.firebase.FirebaseStorageRepo
import com.example.pengepul.repo.firebase.FirebaseTokoRepo
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.core.Repo
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class TokoVM(
    private val auth: FirebaseAuth,
    private val repoToko: FirebaseTokoRepo,
    private val repoStorage: FirebaseStorageRepo,
    private val dispatcher: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    private val _dataTokoLiveData = MutableLiveData<Toko>()
    val dataTokoLiveData: LiveData<Toko> get() = _dataTokoLiveData

    private val _photoLiveData = MutableLiveData<Uri>()
    val photoLiveData: LiveData<Uri> get() = _photoLiveData

    private val _dataPengepul = MutableLiveData<Pengepul>()
    val dataPengepul : LiveData<Pengepul> get() = _dataPengepul

    fun downloadPhoto(nama_toko: String) {
        CoroutineScope(dispatcher.io()).launch {
            try {
                val photo =
                    repoStorage.downloadFotoToko(auth.currentUser?.uid.toString(), nama_toko)
                _photoLiveData.postValue(photo)
            } catch (e: Exception) {

            }
        }
    }

    fun uploadPhoto(nama_toko: String, photo: Uri) {
        CoroutineScope(dispatcher.io()).launch {
            try {
                repoStorage.uploadFotoToko(
                    auth.currentUser?.uid.toString(),
                    nama_toko,
                    photo,
                    onResult = { uri ->
                        _photoLiveData.postValue(uri)
                    })
            } catch (e: Exception) {
                _errorLiveData.postValue(e.message)
                Log.d("Upload Photo: ", e.message.toString())
            }
        }
    }

    fun getDataToko() {
        CoroutineScope(dispatcher.io()).launch {
            try {
                when (val result = repoToko.getDataToko(auth.currentUser?.uid.toString())) {
                    is RepositoryResult.Success -> {
                        Log.d("Get Data", "result: ${result.data}")
                        _dataTokoLiveData.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatcher.main()) {
                        Log.d("Error Get Data", "result: ${result.exception}")
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatcher.main()) {
                        Log.d("Error Get Data", "result: ${result.exception}")
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatcher.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    fun addToko(nama: String, latitude: Double, longitude: Double, address: String, tokenFcm: String, noTelp: String) {
        CoroutineScope(dispatcher.io()).launch {
            try {
                when (val result = repoToko.addDataToko(nama, latitude, longitude, address, tokenFcm, noTelp)) {
                    is RepositoryResult.Success -> {
                        withContext(dispatcher.main()) {
                            _successLiveData.value = "Berhasil Menambahkan Toko"
                        }
                    }
                    is RepositoryResult.Error -> {
                        withContext(dispatcher.main()) {
                            _errorLiveData.value = result.exception.message
                        }
                    }
                    is RepositoryResult.Canceled -> {
                        withContext(dispatcher.main()) {
                            _errorLiveData.value = result.exception?.message
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatcher.main()) {
                    _errorLiveData.value = e.message
                }
            }
        }
    }

    fun openTokoOperation() = CoroutineScope(dispatcher.io()).launch {
        val firestore = Firebase.firestore.collection(Constants.TOKO)
        val dataFirestore = firestore.whereEqualTo("id_pengepul", auth.currentUser?.uid.toString()).get().await()
        if (dataFirestore.documents.isNotEmpty()){
            for (document in dataFirestore){
                try {
                    firestore.document(document.id).update("open", true)
                }catch (e: Exception){
                    withContext(dispatcher.main()){
                        _errorLiveData.postValue("Toko Gagal Dibuka")
                        Log.d("Open Toko", "Proses Gagal")
                        Log.e("Open Toko", "Proses Gagal")
                    }
                }
            }
        }else{
            withContext(dispatcher.main()){
                _errorLiveData.postValue("Tidak Ada Data")
                Log.d("Get Data Toko","Data Tidak ada Atau Kosong")
                Log.e("Get Data Toko", "Data Tidak ada atau kosong")
            }
        }
    }

    fun closeTokoOperation() = CoroutineScope(dispatcher.io()).launch {
        val firestore = Firebase.firestore.collection(Constants.TOKO)
        val dataFirestore = firestore.whereEqualTo("id_pengepul", auth.currentUser?.uid.toString()).get().await()
        if (dataFirestore.documents.isNotEmpty()){
            for (document in dataFirestore){
                try {
                    firestore.document(document.id).update("open", false)
                }catch (e: Exception){
                    withContext(dispatcher.main()){
                        _errorLiveData.postValue("Toko Gagal Dibuka")
                        Log.d("Open Toko", "Proses Gagal")
                        Log.e("Open Toko", "Proses Gagal")
                    }
                }
            }
        }else{
            withContext(dispatcher.main()){
                _errorLiveData.postValue("Tidak Ada Data")
                Log.d("Get Data Toko","Data Tidak ada Atau Kosong")
                Log.e("Get Data Toko", "Data Tidak ada atau kosong")
            }
        }
    }

    fun editToko(nama: String, latitude: Double, longitude: Double, address: String) {
        CoroutineScope(dispatcher.io()).launch {
            try {
                when(val result= repoToko.editDataToko(nama, latitude, longitude, address)){
                    is RepositoryResult.Success-> RepositoryResult.Success("Update Toko Berhasil")
                    is RepositoryResult.Error-> withContext(dispatcher.main()){
                        _errorLiveData.postValue("Update Toko Gagal")
                    }
                    is RepositoryResult.Canceled-> withContext(dispatcher.main()){
                        result.exception.message.let {
                            _errorLiveData.postValue(it)
                        }
                        Log.d("Cancel Toko", "${result.exception.message}")
                    }
                }
            }catch (e: Exception){
                withContext(dispatcher.main()){
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                    Log.d("Exceptio Toko","result: ${e.message}")
                }
            }
        }
    }

    fun getPengepul(){
        CoroutineScope(dispatcher.io()).launch {
            try {
                when(val result = repoToko.getDataPengepul()){
                    is RepositoryResult.Success->{
                        _dataPengepul.postValue(result.data)
                    }
                    is RepositoryResult.Error-> withContext(dispatcher.main()){
                        _errorLiveData.postValue("Gagal Mendapatkan Data")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatcher.main()){
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception){
                withContext(dispatcher.main()){
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoToko: FirebaseTokoRepo,
        private val repoStorage: FirebaseStorageRepo,
        private val dispatcher: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return TokoVM(auth, repoToko, repoStorage, dispatcher) as T
        }
    }
}