package com.example.pengepul.ui.notifications

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pengepul.R
import com.example.pengepul.model.BodyPayloadNotif
import com.example.pengepul.model.NotificationModel
import com.example.pengepul.ui.chat.ActivityChat
import com.example.pengepul.utils.Constants
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_detail_transaksi.*
import kotlinx.android.synthetic.main.activity_notifications.*
import org.koin.android.ext.android.inject

class NotificationsActivity : AppCompatActivity() {

    private lateinit var viewModel: NotificationViewModel
    private val factory by inject<NotificationViewModel.Factory>()
    private var adapterNotif: NotificationAdapter? = null

    lateinit var date: Timestamp
    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)

//        adapterNotif = NotificationAdapter(this@NotificationsActivity)
        auth = FirebaseAuth.getInstance()
        viewModel = ViewModelProvider(this, factory).get(NotificationViewModel::class.java)
        viewModel.errorLiveData.observe(this, onError())
//        viewModel.dataNotification.observe(this, observeGetdata())

        date = Timestamp.now()
//        container_notifications.layoutManager =
//            LinearLayoutManager(this@NotificationsActivity, LinearLayoutManager.VERTICAL, false)
//        container_notifications.setHasFixedSize(true)
//        container_notifications.adapter = adapterNotif


//        adapterNotif.setOnClickListenerCallback(object :
//            NotificationAdapter.OnClickListenerCallback {
//            override fun onClickAccept(notification: NotificationModel, position: Int) {
//                viewModel.updateProccessTransaction(
//                    notification.data.get("id_userRongsok").toString()
//                )
//
//                var id_peng = auth.currentUser?.uid!!
//                var nama_peng = auth.currentUser?.displayName
//                var id_user = notification.data.get("id_userRongsok").toString()
//                BodyPayloadNotif(
//                    BodyPayloadNotif.Data(
//                        id_peng,
//                        id_user,
//                        nama_peng,
//                        "$nama_peng Menyetujui Transaksi Dari Anda",
//                        "Konfirmasi Transaksi",
//                        Constants.SHOP
//                    ),
//                    date,
//                    id_user
//                ).also {
//                    viewModel.addNotif(it,id_user)
//                }
//                Log.d("idUser", "result: ${notification.data.get("id_userRongsok")}")
//                Toast.makeText(this@NotificationsActivity, "Orderan Diterima", Toast.LENGTH_LONG)
//                    .show()
//            }
//
//            override fun onClickReject(notification: NotificationModel, position: Int) {
//                viewModel.cancelProccessTransaction(
//                    notification.data.get("id_userRongsok").toString()
//                )
//
//                var id_peng = auth.currentUser?.uid!!
//                var nama_peng = auth.currentUser?.displayName
//                var id_user = notification.data.get("id_userRongsok").toString()
//                BodyPayloadNotif(
//                    BodyPayloadNotif.Data(
//                        id_peng,
//                        id_user,
//                        nama_peng,
//                        "$nama_peng Menolak Pengajuan Transaksi Dari Anda",
//                        "Konfirmasi Transaksi",
//                        Constants.SHOP
//                    ),
//                    date,
//                    id_user
//                ).also {
//                    viewModel.addNotif(it,id_user)
//                }
//                Log.d("idUser", "Result: ${notification.data.get("id_userRongsok")}")
//                Toast.makeText(this@NotificationsActivity, "Orderan Ditolak", Toast.LENGTH_LONG)
//                    .show()
//            }
//
//            override fun onClickListenerChat(notification: NotificationModel, position: Int) {
//                val intent = Intent(this@NotificationsActivity, ActivityChat::class.java)
//                intent.putExtra("dataChat", notification)
//                startActivity(intent)
//            }
//
//        })
//        showLoading(true)
//        viewModel.getDataNotification()
    }

    override fun onStart() {
        super.onStart()
        initRecycleView()
    }

    private fun initRecycleView() {
        container_notifications.layoutManager = LinearLayoutManager(this)
        container_notifications.setHasFixedSize(true)

        viewModel.getDataNotif()
//        showLoading(false)
        viewModel.dataNotif.observe(this, Observer {
            val options = FirestoreRecyclerOptions.Builder<NotificationModel>()
                .setQuery(it, NotificationModel::class.java).build()

            adapterNotif = NotificationAdapter(options = options)
            container_notifications.adapter = adapterNotif
            adapterNotif?.startListening()
            adapterNotif?.setOnClickListenerCallback(object :
                NotificationAdapter.OnClickListenerCallback {
                override fun onClickAccept(notification: NotificationModel, position: Int) {
                    viewModel.updateProccessTransaction(
                        notification.data.get("id_userRongsok").toString()
                    )
                    var id_peng = auth.currentUser?.uid!!
                    var nama_peng = auth.currentUser?.displayName
                    var id_user = notification.data.get("id_userRongsok").toString()
                    BodyPayloadNotif(
                        BodyPayloadNotif.Data(
                            id_peng,
                            id_user,
                            nama_peng,
                            "$nama_peng Menyetujui Transaksi Dari Anda",
                            "Konfirmasi Transaksi",
                            Constants.SHOP
                        ),
                        date,
                        id_user
                    ).also {
                        viewModel.addNotif(it, id_user)
                        viewModel.sendNotification(it)
                    }
                    Log.d("idUser", "result: ${notification.data.get("id_userRongsok")}")
                    Toast.makeText(
                        this@NotificationsActivity,
                        "Orderan Diterima",
                        Toast.LENGTH_LONG
                    ).show()
                    adapterNotif?.deleteData(position)
                }

                override fun onClickReject(notification: NotificationModel, position: Int) {
                    viewModel.cancelProccessTransaction(
                        notification.data.get("id_userRongsok").toString()
                    )

                    var id_peng = auth.currentUser?.uid!!
                    var nama_peng = auth.currentUser?.displayName
                    var id_user = notification.data.get("id_userRongsok").toString()
                    BodyPayloadNotif(
                        BodyPayloadNotif.Data(
                            id_peng,
                            id_user,
                            nama_peng,
                            "$nama_peng Menolak Pengajuan Transaksi Dari Anda",
                            "Konfirmasi Transaksi",
                            Constants.SHOP
                        ),
                        date,
                        id_user
                    ).also {
                        viewModel.addNotif(it, id_user)
                        viewModel.sendNotification(it)
                    }
                    Log.d("idUser", "Result: ${notification.data.get("id_userRongsok")}")
                    Toast.makeText(this@NotificationsActivity, "Orderan Ditolak", Toast.LENGTH_LONG)
                        .show()
                    adapterNotif?.deleteData(position)
                }

                override fun onClickListenerChat(notification: NotificationModel, posistion: Int) {
                    val intent = Intent(this@NotificationsActivity, ActivityChat::class.java)
                    intent.putExtra("dataChat", notification)
                    startActivity(intent)
                    adapterNotif?.deleteData(posistion)
                }

            })
        })
    }

    override fun onStop() {
        super.onStop()
        adapterNotif?.stopListening()
    }

//    private fun observeGetdata() = Observer<MutableList<NotificationModel>> { list_notif ->
//        runOnUiThread {
//            if (list_notif.isEmpty()) {
//                tv_noNotif.visibility = View.VISIBLE
//                showLoading(false)
//            } else {
//                adapterNotif.setData(list_notif)
//                container_notifications.visibility = View.VISIBLE
//                showLoading(false)
//            }
//        }
//    }

    fun onError() = Observer<String> {
        runOnUiThread {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            Log.e("Error", it)
        }
    }

//    fun showLoading(state: Boolean) {
//        if (state) {
//            pb_notif.visibility = View.VISIBLE
//        } else {
//            pb_notif.visibility = View.GONE
//        }
//    }

}