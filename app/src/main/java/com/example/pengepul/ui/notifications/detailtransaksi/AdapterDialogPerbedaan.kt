package com.example.pengepul.ui.notifications.detailtransaksi

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pengepul.R
import com.example.pengepul.model.RongsokAdapter
import com.example.pengepul.ui.mainmenu.transaksi.RongsokTransaksiAdapter
import kotlinx.android.synthetic.main.item_dialog_perbedaan.view.*
import java.util.ArrayList

class AdapterDialogPerbedaan(
    val context: Context,
    val list_rongsok: MutableList<RongsokTransaksiAdapter>
) : RecyclerView.Adapter<AdapterDialogPerbedaan.listViewHolder>() {
    private lateinit var listener: OnEditListener

    fun setOnEditListener(listenerCallback: OnEditListener){
        this.listener = listenerCallback
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDialogPerbedaan.listViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_dialog_perbedaan, parent, false)
        return listViewHolder(itemView)
    }

    inner class listViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(rongsok: RongsokTransaksiAdapter){
            view.tv_nama_rongsok_awal.text = rongsok.nama_rongsok
            view.tv_harga_rongsok_awal.text = rongsok.harga_rongsok.toString()
            view.et_berat_rongsok.setText(rongsok.quantity.toString())

            listener.let {
                view.et_berat_rongsok.addTextChangedListener(object : TextWatcher{
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {

                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                        if(s.toString()!=""){
                            Log.d("Edit Berat ", s.toString())
                            listener.editBerat(rongsok, s.toString(), layoutPosition)
//                            onEdit.editBeratRongsok(list_rongsok[adapterPosition],s.toString(), adapterPosition)
                        }
                    }

                    override fun afterTextChanged(s: Editable?) {
                    }
                })
            }
        }
    }

    fun setData(list: List<RongsokTransaksiAdapter>){
        this.list_rongsok.clear()
        this.list_rongsok.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: AdapterDialogPerbedaan.listViewHolder, position: Int) {
        holder.bind(list_rongsok[position])
    }

    override fun getItemCount(): Int {
        return list_rongsok.size
    }

    interface OnEditListener{
        fun editBerat(rongsok: RongsokTransaksiAdapter, berat: String, position: Int)
    }
}