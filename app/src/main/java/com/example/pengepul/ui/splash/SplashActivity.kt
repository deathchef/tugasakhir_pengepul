package com.example.pengepul.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.pengepul.R
import com.example.pengepul.ui.login.LoginActivity
import com.example.pengepul.ui.mainmenu.MainMenu
import com.example.pengepul.utils.FirebaseHelper
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {
    private val firebaseHelper by inject<FirebaseHelper>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (firebaseHelper.firebaseLogin()) {
            Handler().postDelayed({
                startActivity(Intent(this, MainMenu::class.java))
                finish()
            }, 3000)
        }else{
            Handler().postDelayed({
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }, 3000)
        }
    }
}