package com.example.pengepul.ui.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.directions.route.*
import com.example.pengepul.R
import com.example.pengepul.ui.mainmenu.profil.ProfilVM
import com.example.pengepul.ui.mainmenu.toko.Toko
import com.example.pengepul.ui.mainmenu.toko.TokoVM
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*

import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import org.koin.android.ext.android.inject
import java.util.*

class LocationUser : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    var toko = Toko()
    private val latitudeUser by lazy {
        intent.getDoubleExtra("latitudeUser", 0.0)
    }
    private val longitudeUser by lazy {
        intent.getDoubleExtra("longitudeUser", 0.0)
    }

    private val nama_user by lazy {
        intent.getStringExtra("nama_user")
    }
    lateinit var placeUser: LatLng


    private lateinit var mapFragment: SupportMapFragment
    lateinit var viewModel : TokoVM
    private val factory by inject<TokoVM.Factory>()
    lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_user)
        auth = FirebaseAuth.getInstance()

        viewModel = ViewModelProvider(this, factory).get(TokoVM::class.java)
        viewModel.dataTokoLiveData.observe(this, observeGetDataToko())
        Log.d("LATLNG","LAT: $latitudeUser, long: $longitudeUser, namaUser: $nama_user")
        viewModel.getDataToko()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager.findFragmentById(R.id.map_user) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun observeGetDataToko() =  Observer<Toko> {
        runOnUiThread {
            this.toko = it

            val placePengepul = LatLng(it.latitude, it.longitude)
            val markerPengepul = MarkerOptions()
                .position(placePengepul)
                .title("Posisi Anda")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
            mMap!!.addMarker(markerPengepul)
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(placeUser))

//        this.toko.let {
//            val placePengepul = LatLng(it.latitude,it.longitude)
//            val markerOptions = MarkerOptions()
//                .position(placePengepul)
//                .title("Posisi Anda")
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
//            mMap.addMarker(markerOptions)
//        }

        placeUser = LatLng(latitudeUser, longitudeUser)
//        Log.d("LATLNG","Lat: $latitudeUser, long: $longitudeUser")
        val markerUser = MarkerOptions()
            .position(placeUser)
            .title("Pelanggan: $nama_user")
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
        mMap.addMarker(markerUser)

        mMap.moveCamera(CameraUpdateFactory.newLatLng(placeUser))
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15f))
        // Add a marker in Sydney and move the camera
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return
                }
            } else {
                Toast.makeText(
                    this,
                    "User has not granted location access permission",
                    Toast.LENGTH_LONG
                ).show()
                finish()
            }
        }
    }
}