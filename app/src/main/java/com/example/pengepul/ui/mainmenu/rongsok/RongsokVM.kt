package com.example.pengepul.ui.mainmenu.rongsok

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.JenisRongsok
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.model.Rongsok
import com.example.pengepul.model.RongsokAdapter
import com.example.pengepul.repo.firebase.FirebaseRongsokRepo
import com.example.pengepul.repo.firebase.FirebaseStorageRepo
import com.example.pengepul.repo.firebase.FirebaseStorageRongsok
import com.example.pengepul.repo.firebase.FirebaseTokoRepo
import com.example.pengepul.ui.mainmenu.toko.Toko
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.util.ArrayList

class RongsokVM(
    private val auth: FirebaseAuth,
    private val repoRongsok: FirebaseRongsokRepo,
    private val repoStorageRongsok: FirebaseStorageRongsok,
    private val repoToko: FirebaseTokoRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _succesLiveData = MutableLiveData<String>()
    val succesLiveData: LiveData<String> get() = _succesLiveData

    private val _dataRongsokLiveData = MutableLiveData<MutableList<RongsokAdapter>>()
    val dataRongsokLiveData: LiveData<MutableList<RongsokAdapter>> get() = _dataRongsokLiveData

    private val _photoLiveData = MutableLiveData<Uri>()
    val photoLiveData: LiveData<Uri> get() = _photoLiveData

    private val _dataTokoLiveData = MutableLiveData<Toko>()
    val dataTokoLiveData: LiveData<Toko> get() = _dataTokoLiveData

    private val _jenisLiveData = MutableLiveData<ArrayList<JenisRongsok>>()
    val jenisLiveData: LiveData<ArrayList<JenisRongsok>> get() = _jenisLiveData

//    fun downloadPhotoRongsok(nama_barang: String) {
//        CoroutineScope(dispatchers.io()).launch {
//            try {
//                val photo = repoStorageRongsok.downloadFotoRongsok(
//                    auth.currentUser?.uid.toString(),
//                    nama_barang
//                )
//                _photoLiveData.postValue(photo)
//            } catch (e: Exception) {
//                _errorLiveData.postValue(e.message.toString())
//            }
//        }
//    }

    fun uploadPhotoRongsok(nama_barang: String, photo: Uri) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                repoStorageRongsok.uploadFotoRongsok(
                    auth.currentUser?.uid.toString(),
                    nama_barang,
                    photo,
                    onResult = { uri ->
                        _photoLiveData.postValue(uri)
                    })
            } catch (e: Exception) {
                _errorLiveData.postValue(e.message)
                Log.d("Upload Photo Rongsok: ", e.message.toString())
            }
        }
    }

    fun getDataRongsok() {
        val dataRongsok = mutableListOf<RongsokAdapter>()
        var allDataRongsok = mutableListOf<Rongsok>()
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoRongsok.getDataRongsok(auth.currentUser?.uid.toString())) {
                    is RepositoryResult.Success -> {
                        Log.d("Get Data Rongsok", "result: ${result.data}")
                        allDataRongsok = result.data
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        Log.e("Error Get Data", "Result: ${result.exception}")
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        Log.d("Error Get Data (cancel)", "Result: ${result.exception}")
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
                allDataRongsok.forEach {
                    val foto_rongsokUri = repoStorageRongsok.downloadFotoRongsok(
                        auth.currentUser?.uid.toString(),
                        it.nama_rongsok
                    )
                    Log.d("foto_rongsokUri", foto_rongsokUri.path!!)
                    val rongsokAdapter = RongsokAdapter(
                        it.foto_rongsok,
                        foto_rongsokUri,
                        it.harga_rongsok,
                        it.jenis_rongsok,
                        it.nama_rongsok,
                        it.id_toko,
                        it.isReady
                    )
                    Log.d("rongsokAdapter", rongsokAdapter.toString())
                    dataRongsok.add(rongsokAdapter)
                }
                CoroutineScope(dispatchers.main()).launch {
                    _dataRongsokLiveData.value = dataRongsok
                }
            } catch (e: Exception) {
                CoroutineScope(dispatchers.main()).launch {
                    _errorLiveData.value = e.message!!
                }
            }
        }
    }

    fun getDataToko() {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoToko.getDataToko(auth.currentUser?.uid.toString())) {
                    is RepositoryResult.Success -> {
                        Log.d("Get Data Toko", "result : ${result.data}")
                        _dataTokoLiveData.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        Log.d("Repo Error", "result : ${result.exception}")
                        _errorLiveData.postValue("Gagal Mendapatkan Data Toko")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        Log.d("Repo Cancel", "resutl : ${result.exception}")
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                    Log.d("Exception Data", "result: Tidak ada Data Toko")
                }
            }
        }
    }

    fun addDataRongsok(nama_rongsok: String, harga: Int, jenis_rongsok: String) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoRongsok.addDataRongsok(nama_rongsok, harga, jenis_rongsok)) {
                    is RepositoryResult.Success -> {
                        withContext(dispatchers.main()) {
                            _succesLiveData.value = "Berhasil Menambahkan Rongsok"
                        }
                    }
                    is RepositoryResult.Error -> {
                        withContext(dispatchers.main()) {
                            _errorLiveData.value = result.exception.message
                        }
                    }
                    is RepositoryResult.Canceled -> {
                        withContext(dispatchers.main()) {
                            _errorLiveData.value = result.exception?.message
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    _errorLiveData.value = e.message
                }
            }
        }
    }

    fun getDataJenis() {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoRongsok.getJenisData()) {
                    is RepositoryResult.Success -> {
                        _jenisLiveData.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.value = it
                    }
                }
            }
        }
    }

    fun readyRongsok(nama_rongsok: String) = CoroutineScope(dispatchers.io()).launch {
        val docRef =
            Firebase.firestore.collection(Constants.TOKO).document(auth.currentUser?.uid.toString())
                .collection(Constants.RONGSOK)
        val dataFirestore = docRef.get().await()
        if (dataFirestore.documents.isNotEmpty()){
            for (document in dataFirestore){
                try {
                    docRef.document(nama_rongsok).update("ready", true)
                }catch (e: Exception){
                    withContext(dispatchers.main()){
                        _errorLiveData.postValue("Status Rongsok Gagal Diupdate")
                        Log.d("Ready Rongsok", "Proses Gagal")
                    }
                }
            }
        }else{
            withContext(dispatchers.main()){
                _errorLiveData.postValue("Data Rongsok Tidak Ada")
                Log.d("Get Data Rongsok", "result: Data Kosong")
            }
        }
    }

    fun unreadyRongsok(nama_rongsok: String) = CoroutineScope(dispatchers.io()).launch {
        val docRef =
            Firebase.firestore.collection(Constants.TOKO).document(auth.currentUser?.uid.toString())
                .collection(Constants.RONGSOK)
        val dataFirestore = docRef.get().await()
        if (dataFirestore.documents.isNotEmpty()){
            for (document in dataFirestore){
                try {
                    docRef.document(nama_rongsok).update("ready", false)
                }catch (e: Exception){
                    withContext(dispatchers.main()){
                        _errorLiveData.postValue("Status Rongsok Gagal Diupdate")
                        Log.d("Ready Rongsok", "Proses Gagal")
                    }
                }
            }
        }else{
            withContext(dispatchers.main()){
                _errorLiveData.postValue("Data Rongsok Tidak Ada")
                Log.d("Get Data Rongsok", "result: Data Kosong")
            }
        }
    }

    fun editHargaRongsok(harga: Int, nama_rongsok: String) =
        CoroutineScope(dispatchers.io()).launch {
            val docRef =
                Firebase.firestore.collection(Constants.TOKO).document(auth.currentUser?.uid!!)
                    .collection(Constants.RONGSOK)
            val dataFirestore = docRef.get().await()
            if (dataFirestore.documents.isNotEmpty()) {
                for (document in dataFirestore) {
                    try {
                        docRef.document(nama_rongsok).update("harga_rongsok", harga)
//                        docRef.document(nama_rongsok).update("nama_rongsok", nama_baru)
                    } catch (e: Exception) {
                        withContext(dispatchers.main()) {
                            _errorLiveData.postValue("Gagal Perbaharui Data Harga Rongsok")
                            Log.d("Update Harga", "Proses Gagal")
                        }
                    }
                }
            } else {
                withContext(dispatchers.main()) {
                    _errorLiveData.postValue("Data Tidak ada")
                    Log.d("Get Data", "Data Kosong")
                }
            }
        }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoRongsok: FirebaseRongsokRepo,
        private val repoStorageRongsok: FirebaseStorageRongsok,
        private val repoToko: FirebaseTokoRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RongsokVM(auth, repoRongsok, repoStorageRongsok, repoToko, dispatchers) as T
        }
    }
}