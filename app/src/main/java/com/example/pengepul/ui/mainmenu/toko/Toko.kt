package com.example.pengepul.ui.mainmenu.toko


data class Toko(
    var foto_toko: String = "",
    var nama_toko: String = "",
    var rating: Double = 0.0,
    var pengunjung: Double = 0.0,
    var id_pengepul: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var address: String = "",
    var tokenFcm: String = "",
    var noTelp: String = "",
    var isOpen: Boolean = false
)