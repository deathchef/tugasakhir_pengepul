package com.example.pengepul.ui.notifications

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.BodyPayloadNotif
import com.example.pengepul.model.NotificationModel
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.firebase.FirebaseNotificationRepo
import com.example.pengepul.services.DataManager
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class NotificationViewModel(
    private val auth: FirebaseAuth,
    private val repoNotification: FirebaseNotificationRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() =  _successLiveData

    private val _dataNotification = MutableLiveData<MutableList<NotificationModel>>()
    val dataNotification: LiveData<MutableList<NotificationModel>> get() = _dataNotification

//    fun getDataNotification() {
//        CoroutineScope(dispatchers.io()).launch {
//            try {
//                when (val result =
//                    repoNotification.getNotificationData(auth.currentUser?.uid.toString())) {
//                    is RepositoryResult.Success -> {
//                        _dataNotification.postValue(result.data)
//                        Log.d("Data Notif", "result: ${result.data}")
//                    }
//                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
//                        _errorLiveData.postValue("Gagal Mengambil Notif")
//                    }
//                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
//                        result.exception?.message.let {
//                            _errorLiveData.postValue(it)
//                        }
//                    }
//                }
//            } catch (e: Exception) {
//                withContext(dispatchers.main()) {
//                    e.message?.let {
//                        _errorLiveData.postValue(it)
//                    }
//                }
//            }
//        }
//    }

    val dataNotif = MutableLiveData<Query>()
    private val compositeDisposable = CompositeDisposable()

    fun getDataNotif(){
        compositeDisposable.add(repoNotification.getNotificationData(auth.currentUser?.uid!!).subscribe({
            dataNotif.postValue(it)
        },{

        }))
    }

    fun sendNotification(notification: BodyPayloadNotif) = DataManager.pushNotif(notification).subscribe({
        Log.d("Success Notif",it.toString())
    },{
        Log.d("Throw Notif",it.toString())
    })

    fun addNotif(notificationData: BodyPayloadNotif, id_user: String){
        CoroutineScope(dispatchers.io()).launch {
            try {
                when(val result =  repoNotification.addDataNotif(notificationData, id_user)){
                    is RepositoryResult.Success ->{
                        _successLiveData.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()){
                        _errorLiveData.postValue("Data notif gagal dikirim")
                    }
                    is RepositoryResult.Canceled-> withContext(dispatchers.main()){
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            }catch (e: Exception){
                withContext(dispatchers.main()){
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    fun updateProccessTransaction(id_user: String) =
        CoroutineScope(dispatchers.io()).launch {
            val firestore = Firebase.firestore.collection(Constants.TRANSAKSI)
            val dataFirestore = firestore.whereEqualTo("id_pengepul", auth.currentUser?.uid.toString())
                .whereEqualTo("id_user", id_user).get().await()
            Log.d("data: ",dataFirestore.documents.toString())
            if (dataFirestore.documents.isNotEmpty()) {
                for (document in dataFirestore) {
                    try {
                        firestore.document(document.id).update("proccess", true)
                        firestore.document(document.id).update("status", "Sedang Diproses")
                    } catch (e: Exception) {
                        withContext(dispatchers.main()) {
                            _errorLiveData.postValue("Konfirmasi Rongsok Gagal")
                            Log.d("Konfirmasi Order","Proses Gagal")
                            Log.e("Konfirmasi Order","Proses Gagal")
                        }
                    }
                }
            } else {
                withContext(dispatchers.main()) {
                    _errorLiveData.postValue("Data Kosong")
                    Log.d("Get Data","Data Kosong")
                    Log.e("Get Data","Data Kosong")
                }
            }
        }

    fun cancelProccessTransaction(id_user: String) =
        CoroutineScope(dispatchers.io()).launch {
            val firestore = Firebase.firestore.collection(Constants.TRANSAKSI)
            val dataFirestore = firestore.whereEqualTo("id_pengepul", auth.currentUser?.uid!!)
                .whereEqualTo("id_user", id_user).get().await()
            if (dataFirestore.documents.isNotEmpty()) {
                for (document in dataFirestore) {
                    try {
                        firestore.document(document.id).update("cancel", true)
                        firestore.document(document.id).update("done", true)
                        firestore.document(document.id).update("rated", true)
                        firestore.document(document.id).update("status", "Transaksi Dibatalkan")
                    } catch (e: Exception) {
                        withContext(dispatchers.main()) {
                            _errorLiveData.postValue("Konfirmasi Rongsok Gagal")
                            Log.d("Cancel Order","Proses Gagal")
                            Log.e("Cancel Order","Proses Gagal")
                        }
                    }
                }
            } else {
                withContext(dispatchers.main()) {
                    _errorLiveData.postValue("Data Kosong")
                    Log.d("Get Data","Data Kosong")
                    Log.e("Get Data","Data Kosong")
                }
            }
        }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoNotification: FirebaseNotificationRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return NotificationViewModel(auth, repoNotification, dispatchers) as T
        }
    }
}