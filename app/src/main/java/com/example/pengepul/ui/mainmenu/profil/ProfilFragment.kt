package com.example.pengepul.ui.mainmenu.profil

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.R
import com.example.pengepul.model.Pengepul
import com.example.pengepul.ui.login.LoginActivity
import com.example.pengepul.utils.FirebaseHelper
import com.google.firebase.auth.FirebaseAuth
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.dialog_edit_profil.view.*
import kotlinx.android.synthetic.main.fragment_profil.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ProfilFragment : Fragment() {

    lateinit var auth: FirebaseAuth
    private lateinit var viewModel: ProfilVM
    private val factory by inject<ProfilVM.Factory>()
    private lateinit var pengepul: Pengepul
    private val firebaseHelper by inject<FirebaseHelper>()
    private lateinit var showDialog: AlertDialog
    private val dialogEditProfil by lazy {
        layoutInflater.inflate(R.layout.dialog_edit_profil, null, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profil, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        auth = FirebaseAuth.getInstance()

        viewModel = ViewModelProvider(this, factory).get(ProfilVM::class.java)
        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.dataPengepul.observe(viewLifecycleOwner, observeGetDataPengepul())
        viewModel.successLiveData.observe(viewLifecycleOwner, onSuccess())

        viewModel.getDataPengepul(auth.currentUser?.uid.toString())

        btn_logout.setOnClickListener {
            firebaseHelper.firebaseSignOut()
            startActivity(Intent(activity, LoginActivity::class.java)).apply {
                Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            Toast.makeText(requireContext(), "Berhasil Logout", Toast.LENGTH_SHORT).show()
            activity?.finish()
        }

        btn_editProfil.setOnClickListener {
            this.let {
                if (dialogEditProfil.parent != null) {
                    val viewGroup = dialogEditProfil.parent as ViewGroup
                    viewGroup.removeView(dialogEditProfil)
                }
                val dialog = AlertDialog.Builder(requireContext())
                dialog.setView(dialogEditProfil)
                showDialog = dialog.create()
                showDialog.show()

                val nama = dialogEditProfil.et_editUsername
                val nomor_handphone = dialogEditProfil.et_editNoHp

                dialogEditProfil.btn_simpanEditProfil.setOnClickListener {
                    if (dialogEditProfil.et_editUsername?.text.toString().isNullOrEmpty()) {
                        Toast.makeText(
                            dialogEditProfil.context,
                            "Nama Tidak Boleh Kosong",
                            Toast.LENGTH_LONG
                        ).show()
                        dialogEditProfil.et_editUsername?.isFocusable = true
                    } else if (dialogEditProfil.et_editNoHp?.text.toString().isNullOrEmpty()) {
                        Toast.makeText(
                            dialogEditProfil.context,
                            "Nomor handphone Tidak Boleh Kosong",
                            Toast.LENGTH_LONG
                        ).show()
                        dialogEditProfil.et_editNoHp?.isFocusable = true
                    } else {
                        Log.d(
                            "Edit Profil",
                            "result: nama: ${nama.text}. no.hp: ${nomor_handphone.text}"
                        )
                        viewModel.updateDataProfil(
                            nama?.text.toString(),
                            nomor_handphone?.text.toString()
                        )
                        showDialog.dismiss()
                    }
                    viewModel.getDataPengepul(auth.currentUser?.uid!!)
                }

                dialogEditProfil.btn_cancelEditProfil.setOnClickListener {
                    showDialog.cancel()
                }
            }
        }
    }

    private fun onSuccess() = Observer<String> {
        Toast.makeText(requireContext(),it, Toast.LENGTH_LONG).show()
    }

    private fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            Log.d(ProfilFragment::class.java.simpleName, "Error : $it")
        }
    }

    private fun observeGetDataPengepul() = Observer<Pengepul> { data ->
        this.pengepul = data
        if (data == null) {
            Toast.makeText(requireContext(), "Gagal Memuat Data Pengepul", Toast.LENGTH_SHORT)
                .show()
        } else {
            tv_username.text = data.name
            tv_nomorHp.text = data.noHp
            tv_emailPengepul.text = auth.currentUser?.email
        }
    }
}