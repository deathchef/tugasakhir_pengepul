package com.example.pengepul.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.firebase.FirebaseAuthRepo
import com.example.pengepul.utils.DispatcherProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.Exception

class RegisVM(private val authRepo: FirebaseAuthRepo, private val dispatcher: DispatcherProvider) :
    ViewModel() {

    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _successLiveData = MutableLiveData<String>()
    val succesLiveData: LiveData<String> get() = _successLiveData

    fun register(nama: String, email: String, password: String, phone: String, tokenFCM: String) {
        CoroutineScope(dispatcher.main()).launch {
            try {
                when (val result = authRepo.register(nama, email, password, phone,tokenFCM)) {
                    is RepositoryResult.Success -> {
                        _successLiveData.value = "Berhasil Mendaftar"
                    }
                    is RepositoryResult.Error -> {
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> {
                        _errorLiveData.postValue(result.exception?.message)
                    }
                }
            } catch (e: Exception) {
                _errorLiveData.postValue(e.message)
            }
        }
    }

    class Factory(
        private val authRepo: FirebaseAuthRepo,
        private val dispatcher: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RegisVM(authRepo, dispatcher) as T
        }
    }
}