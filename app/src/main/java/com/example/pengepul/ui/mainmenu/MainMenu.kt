package com.example.pengepul.ui.mainmenu

import android.Manifest
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.pengepul.R
import com.example.pengepul.ui.notifications.NotificationsActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.layout_main.*
import kotlinx.android.synthetic.main.main.*

class MainMenu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)


        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
            .withListener(object : MultiplePermissionsListener{
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    Log.d("PERMISSION", "CHECK")
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    Log.d("PERMISSION","RATIONAL")
                }
            }).check()

        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(null)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeButtonEnabled(false)
        val navbarConfiguration = findNavController(R.id.nav_host_fragment)
        button_nav.setupWithNavController(navbarConfiguration)

        navbarConfiguration.addOnDestinationChangedListener{controller, destination, arguments ->
            when(destination.id){
                R.id.fragment_toko->{
                btn_back.setImageResource(0)
                    tv_label.setText("Toko")
                    btn_image.setImageResource(0)
                }
                R.id.fragment_transaksi ->{
                btn_back.setImageResource(0)
                    btn_image.setImageResource(R.drawable.ic_baseline_notifications_24)
                    tv_label.setText("Transaksi")
                    btn_image.setOnClickListener {
                        startActivity(Intent(this, NotificationsActivity::class.java))
                    }
                }
                R.id.fragment_rongsok ->{
                    btn_image.setImageResource(0)
                    btn_back.setImageResource(0)
                    tv_label.setText("Rongsok")
                }
                R.id.fragment_profil ->{
                    btn_back.setImageResource(0)
                    btn_image.setImageResource(0)
                    tv_label.setText("Profil")
                }
            }
        }
    }
}