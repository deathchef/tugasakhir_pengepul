package com.example.pengepul.ui.mainmenu.transaksi

import android.net.Uri
import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelTransaksi(
    var catatan: String="",
    var foto_transaksi: String="",
    var id_pengepul:String ="",
    var id_user: String ="",
    var isDone: Boolean = false,
    var isProccess: Boolean = false,
    var isCancel: Boolean = false,
    var status: String ="",
    var jumlah_harga: Int = 0,
    var nama_pelanggan: String = "",
    var nama_toko: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var address: String ="",
    var tanggal_transaksi: Timestamp = Timestamp.now()
): Parcelable

data class RongsokTransaksi(
    var foto_rongsok: String="",
    var nama_rongsok: String ="",
    var id_user: String = "",
    var id_toko: String = "",
    var harga_rongsok: Int = 0,
    var quantity: Int = 0
)

data class RongsokTransaksiAdapter(
    var foto_rongsokUri: Uri,
    var foto_rongsok: String,
    var nama_rongsok: String ="",
    var id_user: String = "",
    var id_toko: String = "",
    var harga_rongsok: Int = 0,
    var quantity: Int =0
)