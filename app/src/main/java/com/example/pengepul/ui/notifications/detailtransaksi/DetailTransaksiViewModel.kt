package com.example.pengepul.ui.notifications.detailtransaksi

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.firebase.FirebaseTransaksiRepo
import com.example.pengepul.ui.mainmenu.transaksi.RongsokTransaksi
import com.example.pengepul.ui.mainmenu.transaksi.RongsokTransaksiAdapter
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.DispatcherProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.lang.Exception

class DetailTransaksiViewModel(
    private val auth: FirebaseAuth,
    private val repoTransaksi: FirebaseTransaksiRepo, private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataRongsok = MutableLiveData<MutableList<RongsokTransaksiAdapter>>()
    val dataRongsok: LiveData<MutableList<RongsokTransaksiAdapter>> get() = _dataRongsok

    private val _photoLiveData = MutableLiveData<Uri>()
    val photoLiveData: LiveData<Uri> get() = _photoLiveData

    fun downloadPhoto(id_user: String, nama_toko: String) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                val photo = repoTransaksi.downloadFotoTransaksi(id_user, nama_toko)
                _photoLiveData.postValue(photo)
            } catch (e: Exception) {
                _errorLiveData.postValue("Gagal Memuat Foto")
            }
        }
    }

    fun getDataDetail(id_user: String) {
        val dataRongsokAdapter = mutableListOf<RongsokTransaksiAdapter>()
        var allDataRongsokTransaksi = mutableListOf<RongsokTransaksi>()
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoTransaksi.getRongsokDetailTransaksi(id_user)) {
                    is RepositoryResult.Success -> {
                        allDataRongsokTransaksi = result.data
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue("Gagal Memuat Data Transaksi")
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
                allDataRongsokTransaksi.forEach {
                    val foto_rongsokUri =
                        repoTransaksi.downloadFotoRongsok(it.id_toko, it.nama_rongsok)
                    val rongsokTransaksiAdapter = RongsokTransaksiAdapter(
                        foto_rongsokUri,
                        it.foto_rongsok,
                        it.nama_rongsok,
                        it.id_user,
                        it.id_toko,
                        it.harga_rongsok,
                        it.quantity
                    )
                    dataRongsokAdapter.add(rongsokTransaksiAdapter)
                }
                CoroutineScope(dispatchers.main()).launch {
                    _dataRongsok.value = dataRongsokAdapter
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    fun editDataRongsok(id_user: String, berat: Int, nama_rongsok: String) =
        CoroutineScope(dispatchers.io()).launch {
            val firestore = Firebase.firestore.collection(Constants.TRANSAKSI).document(id_user)
                .collection(Constants.RONGSOK_TRANSAKSI)
            val dataFirestore = firestore.whereEqualTo("id_toko", auth.currentUser?.uid!!)
                .whereEqualTo("nama_rongsok", nama_rongsok).get().await()
            if (dataFirestore.documents.isNotEmpty()){
                for (document in dataFirestore){
                    try {
                        firestore.document(document.id).update("quantity", berat)
                    }catch (e: Exception){
                        withContext(dispatchers.main()){
                            _errorLiveData.postValue("Update Berat Rongsok Gagal")
                            Log.d("Update Quantity", "Proses Gagal")
                            Log.e("Update Quantity", "Proses Gagal")
                        }
                    }
                }
            }else{
                withContext(dispatchers.main()){
                    _errorLiveData.postValue("Data Kosong")
                    Log.d("Update Quantity", "Data Kosong")
                    Log.e("Update Quantity", "Data Kosong")
                }
            }
        }

    fun doneProccessTransaction(id_user: String, harga_transaksi: Int) =
        CoroutineScope(dispatchers.io()).launch {
            val firestore = Firebase.firestore.collection(Constants.TRANSAKSI)
            val dataFirestore = firestore.whereEqualTo("id_pengepul", auth.currentUser?.uid!!)
                .whereEqualTo("id_user", id_user).get().await()
            if (dataFirestore.documents.isNotEmpty()) {
                for (document in dataFirestore) {
                    try {
                        firestore.document(document.id).update("done", true)
                        firestore.document(document.id).update("proccess", false)
                        firestore.document(document.id).update("status", "Transaksi Telah Selesai")
                        firestore.document(document.id).update("jumlah_harga", harga_transaksi)
                    } catch (e: Exception) {
                        withContext(dispatchers.main()) {
                            _errorLiveData.postValue("Transaksi Gagal Diselesaikan")
                            Log.d("Done Transaksi", "Proses Gagal")
                            Log.e("Done Transaksi", "Proses Gagal")
                        }
                    }
                }
            } else {
                withContext(dispatchers.main()) {
                    _errorLiveData.postValue("Data Kosong")
                    Log.d("Data Transaksi", "Data Kosong")
                    Log.e("Data Transaksi", "Data Kosong")
                }
            }
        }

    class Factory(
        private val auth: FirebaseAuth,
        private val repoTransaksi: FirebaseTransaksiRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DetailTransaksiViewModel(auth, repoTransaksi, dispatchers) as T
        }
    }
}