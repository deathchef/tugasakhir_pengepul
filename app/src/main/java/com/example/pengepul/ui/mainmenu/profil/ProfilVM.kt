package com.example.pengepul.ui.mainmenu.profil

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.model.Pengepul
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.firebase.FirebaseUserRepo
import com.example.pengepul.utils.DispatcherProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class ProfilVM(
    private val repoUser: FirebaseUserRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _errorLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> get() = _errorLiveData

    private val _dataPengepul = MutableLiveData<Pengepul>()
    val dataPengepul: LiveData<Pengepul> get() = _dataPengepul

    private val _successLiveData = MutableLiveData<String>()
    val successLiveData: LiveData<String> get() = _successLiveData

    fun getDataPengepul(pengepulId: String) {
        CoroutineScope(dispatchers.io()).launch {
            try {
                when (val result = repoUser.getDataPengepul(pengepulId)) {
                    is RepositoryResult.Success -> {
                        _dataPengepul.postValue(result.data)
                    }
                    is RepositoryResult.Error -> withContext(dispatchers.main()) {
                        _errorLiveData.postValue(result.exception.message)
                    }
                    is RepositoryResult.Canceled -> withContext(dispatchers.main()) {
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            } catch (e: Exception) {
                withContext(dispatchers.main()) {
                    e.message?.let {
                        _errorLiveData.postValue(it)
                    }
                }
            }
        }
    }

    fun updateDataProfil(nama: String, nomor_handphone: String){
        CoroutineScope(dispatchers.io()).launch {
            try {
                when(val result = repoUser.updateProfil(nama, nomor_handphone)){
                    is RepositoryResult.Success->{
                        _successLiveData.postValue("Data Berhasil diperbaharui")
                        Log.d("Success Update Data", "result: ${result.data}")
                    }
                    is RepositoryResult.Error-> withContext(dispatchers.main()){
                        _errorLiveData.postValue("Gagal Perbaharui Data")
                        Log.d("Error Update", "Gagal perbaharui data")
                    }
                    is RepositoryResult.Canceled-> withContext(dispatchers.main()){
                        result.exception?.message.let {
                            _errorLiveData.postValue(it)
                        }
                    }
                }
            }catch (e: Exception){
                withContext(dispatchers.main()){
                    e.message?.let {
                        _errorLiveData.postValue("Perbaharui Data Gagal")
                        Log.d("Exception Update", "Exception Update")
                    }
                }
            }
        }
    }

    class Factory(
        private val repoUser: FirebaseUserRepo,
        private val dispatchers: DispatcherProvider
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ProfilVM(repoUser, dispatchers) as T
        }
    }
}