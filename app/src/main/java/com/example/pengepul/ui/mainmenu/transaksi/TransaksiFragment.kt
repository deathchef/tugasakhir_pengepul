package com.example.pengepul.ui.mainmenu.transaksi

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pengepul.R
import com.example.pengepul.ui.notifications.detailtransaksi.DetailTransaksi
import kotlinx.android.synthetic.main.fragment_transaksi.*
import org.koin.android.ext.android.inject

class TransaksiFragment : Fragment() {
    private lateinit var viewModel: TransaksiVM
    private val factory by inject<TransaksiVM.Factory>()

    private lateinit var adapterTransaksi: AdapterTransaksi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaksi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapterTransaksi =
            AdapterTransaksi(this@TransaksiFragment.requireContext(), mutableListOf())
        viewModel = ViewModelProvider(this, factory).get(TransaksiVM::class.java)

        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.listDataTransaksi.observe(viewLifecycleOwner, observeGetDataTransaksi())

        container_listTransaksi.layoutManager =
            LinearLayoutManager(
                this@TransaksiFragment.requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
        container_listTransaksi.setHasFixedSize(true)
        container_listTransaksi.adapter = adapterTransaksi
        showLoading(true)
        viewModel.getListTransaksi()

        adapterTransaksi.setOnClickListener(object : AdapterTransaksi.OnClickListenerCallback {
            override fun onClickListenerCallback(transaksi: ModelTransaksi) {
                sendDataIntent(transaksi)
            }

        })

    }

    private fun observeGetDataTransaksi() =
        Observer<MutableList<ModelTransaksi>> { list_transaksi ->
            activity?.runOnUiThread {
                if (list_transaksi.isEmpty()) {
                    tv_noTransaksi.visibility = View.VISIBLE
                    showLoading(false)
                } else {
                    adapterTransaksi.setData(list_transaksi)
                    container_listTransaksi.visibility = View.VISIBLE
                    showLoading(false)
                }
            }
        }

    private fun onError() = Observer<String> {
        activity?.runOnUiThread {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    fun sendDataIntent(transaksi: ModelTransaksi) {
        startActivity(
            Intent(
                requireContext(),
                DetailTransaksi::class.java
            ).putExtra("dataTransaksi", transaksi )
        )
    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_transaksi.visibility = View.VISIBLE
        } else {
            pb_transaksi.visibility = View.GONE
        }
    }

}