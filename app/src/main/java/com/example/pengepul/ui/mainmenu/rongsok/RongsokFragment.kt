package com.example.pengepul.ui.mainmenu.rongsok

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.pengepul.R
import com.example.pengepul.model.JenisRongsok
import com.example.pengepul.model.RongsokAdapter
import com.example.pengepul.ui.mainmenu.toko.Toko
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.dialog_edit_rongsok.view.*
import kotlinx.android.synthetic.main.dialog_tambah_rongsok.*
import kotlinx.android.synthetic.main.dialog_tambah_rongsok.view.*
import kotlinx.android.synthetic.main.dialog_tambahtoko.view.*
import kotlinx.android.synthetic.main.fragment_rongsok.*
import kotlinx.android.synthetic.main.item_rongsok.*
import okhttp3.internal.notifyAll
import org.koin.android.ext.android.inject
import java.util.ArrayList

class RongsokFragment : Fragment() {
    private lateinit var adapter: AdapterRongsok
    private val dialogTambahRongsok by lazy {
        layoutInflater.inflate(R.layout.dialog_tambah_rongsok, null, false)
    }
    private lateinit var showDialog: AlertDialog

    private lateinit var viewModel: RongsokVM
    private val factory by inject<RongsokVM.Factory>()

    private var fileUri: Uri? = null
    private lateinit var uriPhoto: Uri

    lateinit var adapterSpinner: ArrayAdapter<String>
    lateinit var dataSpinner: java.util.ArrayList<JenisRongsok>
    var dataJenis = arrayListOf<String>()

    lateinit var jenisRongsok: TextView
    private val dialog_edit_rongsok by lazy {
        layoutInflater.inflate(R.layout.dialog_edit_rongsok, null, false)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rongsok, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = AdapterRongsok()
        viewModel = ViewModelProvider(this, factory).get(RongsokVM::class.java)

        dataSpinner = arrayListOf()
        adapterSpinner = ArrayAdapter<String>(
            this@RongsokFragment.requireContext(),
            R.layout.support_simple_spinner_dropdown_item
        )

        viewModel.succesLiveData.observe(viewLifecycleOwner, onSuccess())
        viewModel.errorLiveData.observe(viewLifecycleOwner, onError())
        viewModel.dataRongsokLiveData.observe(viewLifecycleOwner, observeGetDataRongsok())
        viewModel.dataTokoLiveData.observe(viewLifecycleOwner, observeGetDataToko())
        viewModel.photoLiveData.observe(viewLifecycleOwner, onSuccessDownloadPhoto())
        viewModel.jenisLiveData.observe(viewLifecycleOwner, observeGetJenisRongsok())

        container_rongsok.layoutManager = LinearLayoutManager(
            this@RongsokFragment.requireContext(), LinearLayoutManager.VERTICAL, false
        )
        container_rongsok.adapter = adapter
        dialogTambahRongsok.spinner_jenis.adapter = adapterSpinner

        dialogTambahRongsok.spinner_jenis.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    jenisRongsok.text = dataJenis.get(position)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    tv_jenisRongsok.text = " "
                }

            }
        adapter.setOnClickListener(object : AdapterRongsok.OnClickListenerCallback {
            override fun onClickListenerCallback(rongsok: RongsokAdapter, position: Int) {
                this@RongsokFragment.let {
                    if (dialog_edit_rongsok.parent != null) {
                        val viewGroup = dialog_edit_rongsok.parent as ViewGroup
                        viewGroup.removeView(dialog_edit_rongsok)
                    }
                    val dialog = AlertDialog.Builder(requireContext())
                    dialog.setView(dialog_edit_rongsok)
                    showDialog = dialog.create()
                    showDialog.show()

                    dialog_edit_rongsok.tv_harga_rongsok.text = rongsok.harga_rongsok.toString()
                    dialog_edit_rongsok.tv_nama_rongsok.text = rongsok.nama_rongsok

                    val harga = dialog_edit_rongsok.et_edit_harga_rongsok

                    dialog_edit_rongsok.btn_simpanEdit.setOnClickListener {
//                        if (dialog_edit_rongsok.et_edit_nama_rongsok.text.toString().isNullOrEmpty()){
//                            Toast.makeText(
//                                dialog_edit_rongsok.context,
//                                "Field harga tidak boleh kosong",
//                                Toast.LENGTH_LONG
//                            ).show()
//                            dialog_edit_rongsok.et_edit_harga_rongsok.isFocusable = true
//                        }
                        if (dialog_edit_rongsok.et_edit_harga_rongsok.text.toString()
                                .isNullOrEmpty()
                        ) {
                            Toast.makeText(
                                dialog_edit_rongsok.context,
                                "Field harga tidak boleh kosong",
                                Toast.LENGTH_LONG
                            ).show()
                            dialog_edit_rongsok.et_edit_harga_rongsok.isFocusable = true
                        } else {
                            Log.d(
                                "Edit Rongsok",
                                "result: Harga ${harga.text}, nama Rongsok: ${rongsok.nama_rongsok}"
                            )
                            viewModel.editHargaRongsok(
                                harga.text.toString().toInt(),
                                rongsok.nama_rongsok
                            )
                            showDialog.dismiss()
                        }
                        Toast.makeText(requireContext(), "Data Rongsok Berhasil Diubah", Toast.LENGTH_LONG).show()
                        viewModel.getDataRongsok()
                    }
                    dialog_edit_rongsok.btn_batalEdit.setOnClickListener {
                        showDialog.cancel()
                    }
                }
            }

            override fun readyItem(rongsok: RongsokAdapter, position: Int) {
                viewModel.readyRongsok(rongsok.nama_rongsok)
            }

            override fun unReadyItem(rongsok: RongsokAdapter, position: Int) {
                viewModel.unreadyRongsok(rongsok.nama_rongsok)
            }
        })

        showLoading(true)
        viewModel.getDataToko()
    }

    private fun observeGetDataToko() = Observer<Toko> {
        activity?.runOnUiThread {
            if (it.id_pengepul == ""){
                showLoading(false)
                viewModel.getDataRongsok()
                btn_tambahDataRongsok.setOnClickListener {
                    this.let {
                        Toast.makeText(requireContext(), "Belum Memiliki Toko", Toast.LENGTH_LONG).show()
                    }
                }
            }else{
                viewModel.getDataRongsok()
                showLoading(false)
                btn_tambahDataRongsok.setOnClickListener {
                    this.let {
                        jenisRongsok = dialogTambahRongsok.tv_jenisRongsok
                        viewModel.getDataJenis()
                        if (dialogTambahRongsok.parent != null) {
                            val viewGroup = dialogTambahRongsok.parent as ViewGroup
                            viewGroup.removeView(dialogTambahRongsok)
                        }
                        val dialog = AlertDialog.Builder(requireContext())
                        dialog.setView(dialogTambahRongsok)
                        showDialog = dialog.create()
                        showDialog.show()

                        dialogTambahRongsok.btn_tambahFotoRongsok.setOnClickListener {
                            CropImage.activity()
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .start(requireActivity(), this@RongsokFragment)
                        }
                        dialogTambahRongsok.btn_tambahRongsok.setOnClickListener {
                            if (dialogTambahRongsok.et_namaRongsok?.text.toString().isNullOrEmpty()) {
                                dialogTambahRongsok.et_namaRongsok?.error =
                                    "Nama Rongsok Tidak Boleh Kosong"
                                dialogTambahRongsok.et_namaRongsok?.isFocusable = true
                            } else if (dialogTambahRongsok.et_hargaRongsok?.text.toString()
                                    .isNullOrEmpty()
                            ) {
                                dialogTambahRongsok.et_hargaRongsok?.error =
                                    "Harga Rongsok Tidak Boleh Kosong"
                                dialogTambahRongsok.et_hargaRongsok.isFocusable = true
                            } else if (dialogTambahRongsok.tv_jenisRongsok?.text.toString()
                                    .isNullOrEmpty()
                            ) {
                                dialogTambahRongsok.tv_jenisRongsok?.error =
                                    "Jenis Rongsok Tidak Boleh Kosong"
                                dialogTambahRongsok.tv_jenisRongsok.isFocusable = true
                            } else if (dialogTambahRongsok.iv_rongsok == null) {
                                Toast.makeText(
                                    dialogTambahRongsok.context,
                                    "Foto Rongsok Tidak Boleh Kosong",
                                    Toast.LENGTH_LONG
                                ).show()
                            } else {
                                observeAddRongsok()
                                Log.d("FOTO", "upload")
                                viewModel.uploadPhotoRongsok(
                                    dialogTambahRongsok.et_namaRongsok.text.toString(),
                                    fileUri!!
                                )
                                showDialog.dismiss()
                            }
                            viewModel.getDataRongsok()
                        }
                        dialogTambahRongsok.btn_batalTambah.setOnClickListener {
                            showDialog.dismiss()
                        }
                    }
                }
            }
        }
    }

    fun observeAddRongsok() {
        val nama_rongsok = dialogTambahRongsok.et_namaRongsok?.text.toString()
        val harga_rongsok = dialogTambahRongsok.et_hargaRongsok?.text.toString().toInt()
        Log.d(
            "Rongsok Baru",
            "nama= ($nama_rongsok), harga=($harga_rongsok), jenis = (${jenisRongsok.text})"
        )
        viewModel.addDataRongsok(nama_rongsok, harga_rongsok, jenisRongsok.text.toString())
    }

    fun observeGetJenisRongsok() = Observer<ArrayList<JenisRongsok>> { list_jenis ->
        activity?.runOnUiThread {
            this.dataSpinner = list_jenis

            dataJenis.clear()
            dataSpinner.forEach {
                dataJenis.add(it.nama_jenis)
                Log.d("Nama Jenis", "result: ${it.nama_jenis}")
            }

            Log.d("Data Jenis", "result: $dataJenis")
            adapterSpinner.addAll(dataJenis)
            adapterSpinner.notifyDataSetChanged()
        }
    }


    fun onSuccessDownloadPhoto() = Observer<Uri> { uri ->
        activity?.runOnUiThread {
            this.uriPhoto = uri
//            Glide.with(this@RongsokFragment.requireContext())
//                .load(uri)
//                .into(iv_photoRongsok!!)
        }
    }

    fun observeGetDataRongsok() = Observer<MutableList<RongsokAdapter>> { list_rongsok ->
        activity?.runOnUiThread {
            if (list_rongsok.isEmpty()) {
                ll_listRongsok.isVisible = false
                showLoading(false)
                tv_noRongsok.isVisible = true
                btn_tambahDataRongsok.visibility  =View.VISIBLE
            } else {
                adapter.setData(list_rongsok)
                ll_listRongsok.isVisible = true
                showLoading(false)
                tv_noRongsok.isVisible = false
                btn_tambahDataRongsok.visibility  =View.VISIBLE

                Log.d("list_rongsok", "result: $list_rongsok")
            }
        }
    }

    fun onSuccess() = Observer<String> {
        showDialog.dismiss()
        Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
    }

    fun onError() = Observer<String> {
        activity?.runOnUiThread {

        }
    }

    fun showLoading(state: Boolean) {
        if (state) {
            pb_rongsok.visibility = View.VISIBLE
        } else {
            pb_rongsok.visibility = View.GONE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val result = CropImage.getActivityResult(data)
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                fileUri = result.uri
                Log.d("Gambar Toko: ", fileUri?.path.toString())
                Glide.with(this@RongsokFragment).load(fileUri).into(dialogTambahRongsok.iv_rongsok)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.e("Pict Toko: ", error.toString())
            }
        }
    }
}
