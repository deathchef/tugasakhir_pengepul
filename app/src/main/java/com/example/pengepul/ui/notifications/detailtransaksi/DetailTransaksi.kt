package com.example.pengepul.ui.notifications.detailtransaksi

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.pengepul.R
import com.example.pengepul.ui.chat.ActivityChat
import com.example.pengepul.ui.mainmenu.MainMenu
import com.example.pengepul.ui.mainmenu.transaksi.ModelTransaksi
import com.example.pengepul.ui.mainmenu.transaksi.RongsokTransaksiAdapter
import com.example.pengepul.ui.maps.LocationUser
import kotlinx.android.synthetic.main.activity_detail_transaksi.*
import kotlinx.android.synthetic.main.dialog_perbedaan_harga.*
import kotlinx.android.synthetic.main.dialog_perbedaan_harga.view.*
import kotlinx.android.synthetic.main.item_dialog_perbedaan.view.*
import org.koin.android.ext.android.inject
import java.util.ArrayList

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DetailTransaksi : AppCompatActivity() {
    private lateinit var adapterDetailTransaksi: AdapterDetailTransaksi
    private lateinit var adapterDialogPerbedaan: AdapterDialogPerbedaan

    private lateinit var viewModel: DetailTransaksiViewModel
    private val factory by inject<DetailTransaksiViewModel.Factory>()
    private val dialog_perbedaan by lazy {
        layoutInflater.inflate(R.layout.dialog_perbedaan_harga, null, false)
    }
    lateinit var daftar_rongsok: MutableList<RongsokTransaksiAdapter>

    private lateinit var showDialog: AlertDialog
    private lateinit var uriPhoto: Uri

    private val dataIntent by lazy {
        intent.getParcelableExtra<ModelTransaksi>("dataTransaksi")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaksi)

        daftar_rongsok = mutableListOf()

        adapterDetailTransaksi = AdapterDetailTransaksi(this, arrayListOf())
        adapterDialogPerbedaan = AdapterDialogPerbedaan(dialog_perbedaan.context, daftar_rongsok)

        viewModel = ViewModelProvider(this, factory).get(DetailTransaksiViewModel::class.java)

        viewModel.errorLiveData.observe(this, onError())
        viewModel.dataRongsok.observe(this, observeGetDataRongsok())
        viewModel.photoLiveData.observe(this, onSuccessDownloadPhoto())

        container_detailTransaksi.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        container_detailTransaksi.adapter = adapterDetailTransaksi

        dialog_perbedaan.container_dialog_perbedaan?.layoutManager =
            LinearLayoutManager(this.dialog_perbedaan.context, LinearLayoutManager.VERTICAL, false)
        dialog_perbedaan.container_dialog_perbedaan?.adapter = adapterDialogPerbedaan

        viewModel.getDataDetail(dataIntent.id_user)
        getDataIntent()

        btn_doneTransaksi.setOnClickListener {
            this.let {

                var tv_harga = dialog_perbedaan.tv_harga_transaksi_baru
                tv_harga.text = dataIntent.jumlah_harga.toString()

                if (dialog_perbedaan.parent != null) {
                    val viewGroup = dialog_perbedaan.parent as ViewGroup
                    viewGroup.removeView(dialog_perbedaan)
                }
                val dialog = AlertDialog.Builder(this)
                dialog.setView(dialog_perbedaan)
                showDialog = dialog.create()
                showDialog.show()

                adapterDialogPerbedaan.setOnEditListener(object :
                    AdapterDialogPerbedaan.OnEditListener {
                    override fun editBerat(
                        rongsok: RongsokTransaksiAdapter,
                        berat: String,
                        position: Int
                    ) {
                        rongsok.quantity = berat.toInt()
                        Log.d("Berat Terbaru", "${rongsok.quantity}")

                        var harga = rongsok.harga_rongsok
                        Log.d("Harga Rongsok ", harga.toString())

                        var harga_transaksi = 0
//                        harga_transaksi = harga_transaksi+(rongsok.quantity * rongsok.harga_rongsok)
                        daftar_rongsok.forEach {
                            harga_transaksi = harga_transaksi + (it.quantity * it.harga_rongsok)
                        }
                        Log.d("Harga trans Terbaru ", harga_transaksi.toString())

                        tv_harga.text = harga_transaksi.toString()
                    }

                })

                dialog_perbedaan.btn_ubah_harga.setOnClickListener {
                    viewModel.doneProccessTransaction(
                        dataIntent.id_user,
                        tv_harga.text.toString().toInt()
                    )

//                    var nama_rongsok = ""
//                    daftar_rongsok.forEach {
//                        nama_rongsok = it.nama_rongsok
//                    }

//                    viewModel.editDataRongsok(dataIntent.id_user, dialog_perbedaan.et_berat_rongsok.text.toString().toInt(), nama_rongsok)
                    daftar_rongsok.forEach {
                        viewModel.editDataRongsok(
                            dataIntent.id_user,
                            it.quantity,
                            it.nama_rongsok
                        )
                        Log.d(
                            "Data Rongsok ",
                            "berat terbaru : ${dataIntent.id_user} ${it.quantity}, nama Rongsok: ${it.nama_rongsok}"
                        )
                    }
                    Log.d("Data Transaksi ", "Harga Transaksi Terbaru: ${tv_harga.text}")
                    startActivity(Intent(this, MainMenu::class.java).also {
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
                        Intent.FLAG_ACTIVITY_NEW_TASK
                        Toast.makeText(this, "transaksi Selesai", Toast.LENGTH_LONG).show()
                    })
                }

                dialog_perbedaan.btn_cancel_harga.setOnClickListener {
                    showDialog.dismiss()
                }
            }

//            viewModel.doneProccessTransaction(dataIntent.id_user)
//            startActivity(Intent(this, MainMenu::class.java).also {
//                Intent.FLAG_ACTIVITY_CLEAR_TOP
//                Toast.makeText(this, "Transaksi Selesai", Toast.LENGTH_LONG).show()
//            })
        }

        btn_route_map.setOnClickListener {
            val intent = Intent(this, LocationUser::class.java)
            intent.putExtra("latitudeUser", dataIntent.latitude)
            intent.putExtra("longitudeUser", dataIntent.longitude)
            intent.putExtra("nama_user", dataIntent.nama_pelanggan)
            startActivity(intent)
        }
        btn_chat.setOnClickListener {
            val intent = Intent(this, ActivityChat::class.java)
            intent.putExtra("dataChat", dataIntent)
            startActivity(intent)
        }
    }

    private fun onSuccessDownloadPhoto() = Observer<Uri> { uri ->
        this.uriPhoto = uri
        Glide.with(this)
            .load(uri)
            .into(iv_foto_rongsokTransaksi)
    }

    private fun getDataIntent() {
        tv_catatanTransaksi.text = dataIntent.catatan
        tv_status_detailTransaksi.text = dataIntent.status
        tv_total_hargaDetail.text = dataIntent.jumlah_harga.toString()
        tv_alamatPelangganNotif.text = dataIntent.address
        viewModel.downloadPhoto(dataIntent.id_user, dataIntent.nama_toko)
    }

    private fun observeGetDataRongsok() =
        Observer<MutableList<RongsokTransaksiAdapter>> { list_rongsok ->
            runOnUiThread {
                this.daftar_rongsok = list_rongsok
                if (list_rongsok.isEmpty()) {
                    Toast.makeText(this, "Tidak Ada Item Rongsok", Toast.LENGTH_LONG).show()
                    Log.d("List Rongsok", "result: $list_rongsok")
                } else {
                    adapterDetailTransaksi.setData(list_rongsok)
                    adapterDialogPerbedaan.setData(daftar_rongsok)
                }
            }
        }

    private fun onError() = Observer<String> {
        runOnUiThread {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
    }
}