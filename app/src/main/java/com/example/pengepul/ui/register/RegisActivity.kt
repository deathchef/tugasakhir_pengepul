package com.example.pengepul.ui.register

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.pengepul.R
import com.example.pengepul.ui.login.LoginActivity
import com.example.pengepul.utils.FirebaseHelper
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.messaging.FirebaseMessaging
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_regis.*
import org.koin.android.ext.android.inject

class RegisActivity : AppCompatActivity() {
    private val REG = RegisActivity::class.java.simpleName
    private val firebaseHelper by inject<FirebaseHelper>()
    private lateinit var viewModel: RegisVM
    private val factory: RegisVM.Factory by inject()

    private val dialog: AlertDialog by lazy {
        SpotsDialog.Builder().setContext(this).build()
    }
    lateinit var tokenFCM: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_regis)

        viewModel = ViewModelProvider(this, factory).get(RegisVM::class.java)
        viewModel.errorLiveData.observe(this, onError())
        viewModel.succesLiveData.observe(this, onSuccess())

        btn_regis.setOnClickListener {
            showLoading(true)
            registerUser()
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task->
            if (!task.isSuccessful) {
                Log.w("Token", "Fetching FCM registration token failed", task.exception)
                return@addOnCompleteListener
            }

            // Get new FCM registration token
            tokenFCM = task.result ?: ""
            Log.d("Token", tokenFCM)
        }
    }

    private fun onSuccess() = Observer<String> {
        runOnUiThread {
            showLoading(false)
            firebaseHelper.firebaseSignOut()
            startActivity(Intent(this, LoginActivity::class.java)).apply {
                Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun onError() = Observer<String> {
        Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        showLoading(false)
    }

    private fun registerUser() {
        val username = et_daftar_nama.text.toString().trim { it <= ' ' }
        val email = et_daftar_email_user.text.toString().trim { it <= ' ' }
        val password = et_password_daftar_user.text.toString().trim { it <= ' ' }
        val noHp = et_daftar_noTelp.text.toString().trim { it <= ' ' }

        when {
            username.isEmpty() -> {
                showError("Kolom Nama Tidak Boleh Kosong")
                showLoading(false)
            }
            email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                showError("Email Tidak Valid")
                showLoading(false)
            }
            password.isEmpty() || password.length < 8 -> {
                showError("Password Tidak Boleh Kosong atau Kurang Dari 8 Karakter")
                showLoading(false)
            }
            noHp.isNullOrEmpty() -> {
                showError("Kolom Nomor Handphone Tidak Boleh Kosong")
                showLoading(false)
            }
            else -> {
                viewModel.register(username, email, password, noHp, tokenFCM)
                Log.d(
                    REG,
                    "Informasi Pendaftar: Nama($username), Email($email), Nomor Handphone($noHp)"
                )
            }
        }
    }

    private fun showError(msg: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.errorMsg))
        snackbar.setTextColor(ContextCompat.getColor(this, R.color.teksPutih))
        snackbar.show()
    }
    fun showLoading(state:Boolean){
        if (state){
            pb_regis.visibility = View.VISIBLE
        }else{
            pb_regis.visibility = View.GONE
        }
    }
}