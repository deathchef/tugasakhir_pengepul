package com.example.pengepul.ui.mainmenu.transaksi

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pengepul.R
import kotlinx.android.synthetic.main.item_transaksi.view.*

class AdapterTransaksi(var context: Context, private val data: MutableList<ModelTransaksi> = mutableListOf()) :
    RecyclerView.Adapter<AdapterTransaksi.listTransaksiHolder>() {
    private lateinit var listener: OnClickListenerCallback

    fun setOnClickListener(listenerCallback: OnClickListenerCallback){
        this.listener = listenerCallback
    }
    inner class listTransaksiHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(transaksi: ModelTransaksi){
            itemView.tv_unameTransaksi.text = transaksi.nama_pelanggan
            itemView.tv_hargaTransaksi.text = transaksi.jumlah_harga.toString()
            itemView.tv_statusTransaksi.text = transaksi.status

            listener.let {
                itemView.setOnClickListener {
                    listener.onClickListenerCallback(transaksi)
                }
            }
        }
    }
    fun setData(data: MutableList<ModelTransaksi>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): listTransaksiHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_transaksi, parent, false)
        return listTransaksiHolder(view)
    }

    override fun onBindViewHolder(holder: listTransaksiHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    interface OnClickListenerCallback{
        fun onClickListenerCallback(transaksi: ModelTransaksi)
    }
}