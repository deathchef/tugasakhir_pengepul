package com.example.pengepul.model

data class Pengepul(
    var id: String = "",
    var name: String = "",
    var email: String = "",
    var password: String = "",
    var noHp: String = "",
    var tokenFcm : String="",
    var isBanned: Boolean = false
)