package com.example.pengepul.model

import android.net.Uri

data class Rongsok(
    var foto_rongsok: String = "",
    var harga_rongsok: Int = 0,
    var jenis_rongsok: String = "",
    var nama_rongsok: String = "",
    var id_toko: String = "",
    var isReady: Boolean =false
)

data class RongsokAdapter(
    var foto_rongsok: String,
    var foto_rongsokUri: Uri,
    var harga_rongsok: Int = 0,
    var jenis_rongsok: String = "",
    var nama_rongsok: String = "",
    var id_toko: String ="",
    var isReady: Boolean = false
)