package com.example.pengepul.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BodyPayloadNotif(
    val data: Data? = null,
    val date: Timestamp? = null,
    val to: String? = null
):Parcelable {
    @Parcelize
    data class Data(
        val id_Pengepul: String? = null,
        val id_userRongsok: String? = null,
        val nama_pengepul: String? = null,
        val message: String? = null,
        val tittle: String? = null,
        val type: String? = null
    ):Parcelable
}

@Parcelize
data class NotificationModel(
    var data: Map<String, String> = mutableMapOf(),
    var date: Timestamp? = null,
    var to: String = "",
): Parcelable