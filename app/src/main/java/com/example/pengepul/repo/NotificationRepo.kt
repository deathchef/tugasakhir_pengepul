package com.example.pengepul.repo

import com.example.pengepul.model.BodyPayloadNotif
import com.example.pengepul.model.NotificationModel
import com.example.pengepul.model.RepositoryResult
import com.google.firebase.firestore.Query
import io.reactivex.Single

interface NotificationRepo {
    fun getNotificationData(id_pengepul: String): Single<Query>
//    suspend fun updateStatusTransaction(id_pengepul: String, id_user: String)
    suspend fun addDataNotif(notificationData: BodyPayloadNotif, id_user: String): RepositoryResult<String>
}