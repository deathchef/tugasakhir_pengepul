package com.example.pengepul.repo.firebase

import android.net.Uri
import android.widget.Toast
import com.example.pengepul.repo.StorageRepo
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.tasks.await

class FirebaseStorageRepo(private val mStorageRef: StorageReference) : StorageRepo {
    override suspend fun downloadFotoToko(uid: String, nama_toko: String): Uri {
        val ref = mStorageRef.child("toko/$uid$nama_toko.jpg")
        return ref.downloadUrl.await()
    }

    override suspend fun uploadFotoToko(
        uid: String,
        nama_toko: String,
        foto_toko: Uri,
        onResult: (Uri) -> Unit
    ) {
        val ref = mStorageRef.child("toko/$uid$nama_toko.jpg")
        ref.putFile(foto_toko)
            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> {
                return@Continuation ref.downloadUrl
            }).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    if (downloadUri != null) {
                        onResult(downloadUri)
                    }
                }
            }
    }
}