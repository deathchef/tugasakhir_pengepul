package com.example.pengepul.repo.firebase

import com.example.pengepul.model.Pengepul
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.UserRepo
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.FirebaseTaskExtention.awaits
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase

class FirebaseUserRepo(private val auth: FirebaseAuth): UserRepo {
    override suspend fun getDataPengepul(pengepulId: String): RepositoryResult<Pengepul> {
        val firestore = Firebase.firestore.collection(Constants.PENGEPUL)
            .whereEqualTo("id", pengepulId)
        var data = Pengepul()
        return when(val query = firestore.get().awaits()){
            is RepositoryResult.Success ->{
                if (query.data.isEmpty){
                    RepositoryResult.Success(data)
                }else{
                    val pengepul = query.data.toObjects<Pengepul>()
                    data = pengepul[0]
                }
                RepositoryResult.Success(data)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun updateProfil(
        nama: String,
        nomorHandphone: String
    ): RepositoryResult<String> {
        val docRef = Firebase.firestore.collection(Constants.PENGEPUL).document(auth.currentUser?.uid.toString())

        val user = mapOf<String, String>("name" to nama, "noHp" to nomorHandphone)
        return when (val query = docRef.set(user, SetOptions.merge()).awaits()){
            is RepositoryResult.Success->RepositoryResult.Success("Data Berhasil Diperbaharui")
            is RepositoryResult.Error-> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled->RepositoryResult.Canceled(query.exception)
        }
    }
}