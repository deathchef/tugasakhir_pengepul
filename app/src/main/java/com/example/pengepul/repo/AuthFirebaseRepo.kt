package com.example.pengepul.repo

import com.example.pengepul.model.RepositoryResult

interface AuthFirebaseRepo {
    suspend fun login(email: String, password: String): RepositoryResult<String>
    suspend fun register(nama: String, email: String, password: String, phone: String, tokenFCM: String): RepositoryResult<String>
}