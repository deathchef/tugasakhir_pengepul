package com.example.pengepul.repo

import com.example.pengepul.model.JenisRongsok
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.model.Rongsok
import java.util.ArrayList

interface RongsokRepo {
    suspend fun getDataRongsok(uid: String): RepositoryResult<MutableList<Rongsok>>
    suspend fun addDataRongsok(namaRongsok: String, harga: Int, jenis_rongsok: String): RepositoryResult<String>
    suspend fun getJenisData(): RepositoryResult<ArrayList<JenisRongsok>>
}