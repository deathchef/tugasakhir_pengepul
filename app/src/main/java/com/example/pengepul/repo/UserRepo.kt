package com.example.pengepul.repo

import com.example.pengepul.model.Pengepul
import com.example.pengepul.model.RepositoryResult

interface UserRepo {
    suspend fun getDataPengepul(pengepulId:String): RepositoryResult<Pengepul>
    suspend fun updateProfil(nama: String, nomorHandphone: String): RepositoryResult<String>
}