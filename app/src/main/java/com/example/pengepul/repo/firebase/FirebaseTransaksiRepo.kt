package com.example.pengepul.repo.firebase

import android.net.Uri
import android.util.Log
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.TransaksiRepo
import com.example.pengepul.ui.mainmenu.transaksi.ModelTransaksi
import com.example.pengepul.ui.mainmenu.transaksi.RongsokTransaksi
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.FirebaseTaskExtention.awaits
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await

class FirebaseTransaksiRepo(
    private val mStorage: StorageReference,
    private val auth: FirebaseAuth
) : TransaksiRepo {
    override suspend fun getDataTransaksi(id_pengepul: String): RepositoryResult<MutableList<ModelTransaksi>> {
        val firestore = Firebase.firestore.collection(Constants.TRANSAKSI)
            .whereEqualTo("id_pengepul", id_pengepul)
            .whereEqualTo("proccess", true)
            .whereEqualTo("done", false)
            .whereEqualTo("cancel", false)

        var list_transaksi = mutableListOf<ModelTransaksi>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(list_transaksi)
                    Log.d("Get Data Transaksi", "Result: Tidak Ada Data")
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<ModelTransaksi>()
                        data?.let {
                            list_transaksi.add(data)
                        }
                        Log.d("Get Data Transaksi", "Result: ${data.toString()}")
                    }
                }
                RepositoryResult.Success(list_transaksi)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun getRongsokDetailTransaksi(
        id_user: String
    ): RepositoryResult<MutableList<RongsokTransaksi>> {
        val firestore =
            Firebase.firestore.collection(Constants.TRANSAKSI).document(id_user).collection(Constants.RONGSOK_TRANSAKSI)
                .whereEqualTo("id_toko", auth.currentUser?.uid.toString())
        var listRongsok = mutableListOf<RongsokTransaksi>()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(listRongsok)
                } else {
                    query.data.documents.forEach { doc->
                        val data = doc.toObject<RongsokTransaksi>()
                        data?.let {
                            listRongsok.add(data)
                        }
                        Log.d("Get Data Detail", "Result: ${data.toString()}")
                    }
                }
                RepositoryResult.Success(listRongsok)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }
//    override suspend fun getRongsokDetailTransaksi(id_user: String): RepositoryResult<MutableList<RongsokTransaksi>> {
//        val firestore = Firebase.firestore.collection(Constants.TRANSAKSI).document(id_user)
//            .collection(Constants.RONGSOK_TRANSAKSI).whereEqualTo("id_toko", auth.currentUser?.uid!!)
//        var list_rongsok = mutableListOf<RongsokTransaksi>()
//        return when(val query = firestore.get().awaits()){
//            is RepositoryResult.Success ->{
//                if (query.data.isEmpty){
//                    RepositoryResult.Success(list_rongsok)
//                    Log.d("Data Detail Transaksi", "Gagal Memuat Rongsok")
//                }else{
//                    query.data.documents.forEach {doc->
//                        val data = doc.toObject<RongsokTransaksi>()
//                        data?.let {
//                            list_rongsok.add(data)
//                        }
//                        Log.d("Get Rongsok Detail","Result: ${data.toString()}")
//                    }
//                }
//                RepositoryResult.Success(list_rongsok)
//            }
//            is RepositoryResult.Error-> RepositoryResult.Error(query.exception)
//            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
//        }
//    }

    override suspend fun downloadFotoRongsok(id_toko: String, nama_rongsok: String): Uri {
        val ref = mStorage.child("/rongsok/$id_toko$nama_rongsok.jpg")
        return ref.downloadUrl.await()
    }

//    override suspend fun downloadFotoRongsok(nama_rongsok: String): Uri {
//        val ref = mStorage.child("/rongsok${auth.currentUser?.uid!!}$nama_rongsok.jpg")
//        return ref.downloadUrl.await()
//    }

//    override suspend fun downloadFotoTransaksi(id_user: String, nama_toko: String): Uri {
//        val ref = mStorage.child("/transaksi$id_user$nama_toko.jpg")
//        return ref.downloadUrl.await()
//    }
    override suspend fun downloadFotoTransaksi(id_user: String, nama_toko: String): Uri {
        val ref = mStorage.child("transaksi/$id_user$nama_toko.jpg")
        return ref.downloadUrl.await()
    }
}