package com.example.pengepul.repo.firebase

import android.util.Log
import com.example.pengepul.model.JenisRongsok
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.model.Rongsok
import com.example.pengepul.model.RongsokAdapter
import com.example.pengepul.repo.RongsokRepo
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.FirebaseTaskExtention.awaits
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import java.util.ArrayList

class FirebaseRongsokRepo(private val auth: FirebaseAuth) : RongsokRepo {

    var uid = auth.currentUser?.uid!!

    override suspend fun getDataRongsok(uid: String): RepositoryResult<MutableList<Rongsok>> {
        val listRongsok = mutableListOf<Rongsok>()
        val firestore =
            Firebase.firestore.collection(Constants.TOKO).document(uid).collection(Constants.RONGSOK)
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(listRongsok)
                    Log.d(
                        FirebaseRongsokRepo::class.java.simpleName,
                        "Gagal Mengambil Data Rongsok"
                    )
                } else {
                    query.data.documents.forEach { doc ->
                        val data = doc.toObject<Rongsok>()
                        data?.let {
                            listRongsok.add(data)
                        }
                    }
                }
                RepositoryResult.Success(listRongsok)
            }
            is RepositoryResult.Error-> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled ->RepositoryResult.Canceled(query.exception)
        }
    }


    override suspend fun addDataRongsok(
        namaRongsok: String,
        harga: Int,
        jenis_rongsok: String
    ): RepositoryResult<String> {
        val firestore =
            Firebase.firestore.collection(Constants.TOKO).document(uid).collection(Constants.RONGSOK)
        val rongsok = Rongsok(
            foto_rongsok = "rongsok/$uid$namaRongsok",
            harga_rongsok = harga,
            jenis_rongsok = jenis_rongsok,
            nama_rongsok = namaRongsok,
            id_toko = uid,
            isReady = false
        )
        return when (val query = firestore.document(namaRongsok).set(rongsok, SetOptions.merge()).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Data Rongsok Berhasil Ditambah")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
    }

    override suspend fun getJenisData(): RepositoryResult<ArrayList<JenisRongsok>> {
        val firestore = Firebase.firestore.collection(Constants.JENIS)
        val listJenis = arrayListOf<JenisRongsok>()

        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    Log.w(
                        FirebaseRongsokRepo::class.java.simpleName,
                        "Gagal Memuat Data Jenis Rongsok"
                    )
                    RepositoryResult.Success(listJenis)
                } else {
                    query.data.documents.forEach {
                        val data = it.toObject<JenisRongsok>()
                        data?.let {
                            listJenis.add(data)
                        }
                    }
                }
                RepositoryResult.Success(listJenis)
            }
            is RepositoryResult.Error -> {
                RepositoryResult.Error(query.exception)

            }
            is RepositoryResult.Canceled -> {
                RepositoryResult.Canceled(query.exception)
            }
        }
    }
}