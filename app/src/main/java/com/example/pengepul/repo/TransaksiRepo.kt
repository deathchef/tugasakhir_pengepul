package com.example.pengepul.repo

import android.net.Uri
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.ui.mainmenu.transaksi.ModelTransaksi
import com.example.pengepul.ui.mainmenu.transaksi.RongsokTransaksi

interface TransaksiRepo {
    suspend fun getDataTransaksi(id_pengepul: String): RepositoryResult<MutableList<ModelTransaksi>>
    suspend fun getRongsokDetailTransaksi(id_user: String): RepositoryResult<MutableList<RongsokTransaksi>>
    suspend fun downloadFotoRongsok(id_toko: String, nama_rongsok: String): Uri
    suspend fun downloadFotoTransaksi(id_user: String, nama_toko: String): Uri
}