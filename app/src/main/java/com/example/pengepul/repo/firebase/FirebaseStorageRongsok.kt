package com.example.pengepul.repo.firebase

import android.net.Uri
import com.example.pengepul.repo.StorageRongsokRepo
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.coroutines.tasks.await

class FirebaseStorageRongsok(private val mStorageRef: StorageReference): StorageRongsokRepo {

    override suspend fun downloadFotoRongsok(uid: String, nama_barang: String): Uri {
        val ref = mStorageRef.child("rongsok/$uid$nama_barang.jpg")
        return ref.downloadUrl.await()
    }

    override suspend fun uploadFotoRongsok(
        uid: String,
        nama_barang: String,
        foto_rongsok: Uri,
        onResult: (Uri) -> Unit
    ) {
        val ref = mStorageRef.child("rongsok/$uid$nama_barang.jpg")
        ref.putFile(foto_rongsok)
            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> {
                return@Continuation ref.downloadUrl
            }).addOnCompleteListener {task->
                if (task.isSuccessful){
                    val downloadUri = task.result
                    if (downloadUri != null){
                        onResult(downloadUri)
                    }
                }
            }
    }
}