package com.example.pengepul.repo.firebase

import android.util.Log
import com.example.pengepul.model.BodyPayloadNotif
import com.example.pengepul.model.NotificationModel
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.NotificationRepo
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.FirebaseTaskExtention.awaits
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import io.reactivex.Single
import kotlinx.coroutines.tasks.await

class FirebaseNotificationRepo : NotificationRepo {
    override fun getNotificationData(id_pengepul: String): Single<Query> = Single.create {
        val firestore = Firebase.firestore.collection(Constants.NOTIFICATIONS).document(id_pengepul)
            .collection("data").orderBy("date",Query.Direction.DESCENDING)
        it.onSuccess(firestore)
    }
//    override suspend fun getNotificationData(id_pengepul: String): RepositoryResult<MutableList<NotificationModel>> {
//        val firestore = Firebase.firestore.collection(Constants.NOTIFICATIONS).document(id_pengepul)
//            .collection("data")
////            .whereEqualTo("done", false)
//            .orderBy("date", Query.Direction.DESCENDING)
//        val list_notif = mutableListOf<NotificationModel>()
//
//        return when (val query = firestore.get().awaits()) {
//            is RepositoryResult.Success -> {
//                if (query.data.isEmpty){
//                    RepositoryResult.Success(list_notif)
//                    Log.d("Get Data Notif","Gagal Mengambil Data Notif")
//                }else{
//                    query.data.documents.forEach {doc->
//                        val data = doc.toObject<NotificationModel>()
//                        data?.let {
//                            list_notif.add(data)
//                        }
//                    }
//                }
//                RepositoryResult.Success(list_notif)
//            }
//            is RepositoryResult.Error -> {
//                Log.e("Error","${query.exception}")
//                RepositoryResult.Error(query.exception)
//            }
//            is RepositoryResult.Canceled -> {
//                RepositoryResult.Canceled(query.exception)
//            }
//        }
//    }

    override suspend fun addDataNotif(
        notificationData: BodyPayloadNotif,
        id_user: String
    ): RepositoryResult<String> {
        val firestore = Firebase.firestore.collection(Constants.NOTIFICATIONS).document(id_user)
            .collection("data")
        return when (val query = firestore.add(notificationData).awaits()) {
            is RepositoryResult.Success -> {
                RepositoryResult.Success("Notifikasi Terkirim")
            }
            is RepositoryResult.Error -> {
                RepositoryResult.Error(query.exception)
            }
            is RepositoryResult.Canceled -> {
                query.exception?.let {
                    RepositoryResult.Canceled(it)
                }
            }
        }
    }
}