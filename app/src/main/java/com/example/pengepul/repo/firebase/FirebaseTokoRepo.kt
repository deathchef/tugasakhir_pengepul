package com.example.pengepul.repo.firebase

import android.util.Log
import com.example.pengepul.model.Pengepul
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.TokoRepo
import com.example.pengepul.ui.mainmenu.toko.Toko
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.FirebaseTaskExtention.awaits
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import java.util.*

class FirebaseTokoRepo(private val auth: FirebaseAuth) : TokoRepo {

    val uid = auth.currentUser?.uid!!.toString()

    override suspend fun getDataToko(uid: String): RepositoryResult<Toko> {
        val firestore =
            Firebase.firestore.collection(Constants.TOKO).whereEqualTo("id_pengepul", uid)
        var toko = Toko()
        return when (val query = firestore.get().awaits()) {
            is RepositoryResult.Success -> {
                if (query.data.isEmpty) {
                    RepositoryResult.Success(toko)
                    Log.w(FirebaseTokoRepo::class.java.simpleName, "Data Toko Gagal Dimuat")
                } else {
                    val data = query.data.toObjects<Toko>()
                    toko = data[0]
                    Log.d(FirebaseTokoRepo::class.java.simpleName, "Data Toko: $toko")
                }
                RepositoryResult.Success(toko)
            }
            is RepositoryResult.Error -> {
                RepositoryResult.Error(query.exception)
            }
            is RepositoryResult.Canceled -> {
                RepositoryResult.Canceled(query.exception)
            }
        }
    }

    override suspend fun addDataToko(
        nama: String,
        latitude: Double,
        longitude: Double,
        address: String,
        tokenFcm: String,
        noTelp: String
    ): RepositoryResult<String> {
        val docRef = Firebase.firestore.collection(Constants.TOKO)
        val toko = Toko(
            foto_toko = "toko/$uid$nama.jpg",
            nama_toko = nama,
            rating = 0.0,
            pengunjung = 0.0,
            id_pengepul = uid,
            latitude = latitude,
            longitude = longitude,
            address = address,
            tokenFcm = tokenFcm,
            noTelp = noTelp
        )
        return when (val query = docRef.document(uid).set(toko, SetOptions.merge()).awaits()) {
            is RepositoryResult.Success -> {
                RepositoryResult.Success("Toko Berhasil Dibuat")
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
    }

    override suspend fun getDataPengepul(): RepositoryResult<Pengepul> {
        val firestore = Firebase.firestore.collection(Constants.PENGEPUL)
            .whereEqualTo("id", auth.currentUser?.uid!!)
        var data = Pengepul()
        return when(val query = firestore.get().awaits()){
            is RepositoryResult.Success->{
                if (query.data.isEmpty){
                    Log.d("Data pengepul", "Gagal Mendapat Data")
                    RepositoryResult.Success(data)
                }else{
                    val pengepul = query.data.toObjects<Pengepul>()
                    data = pengepul[0]
                }
                RepositoryResult.Success(data)
            }
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> RepositoryResult.Canceled(query.exception)
        }
    }

    override suspend fun editDataToko(
        nama: String,
        latitude: Double,
        longitude: Double,
        address: String
    ): RepositoryResult<String> {
        val docRef = Firebase.firestore.collection(Constants.TOKO)
        val toko = mapOf("nama_toko" to nama, "latitude" to latitude, "longitude" to longitude, "address" to address)

        return when(val query = docRef.document(uid).set(toko, SetOptions.merge()).awaits()){
            is RepositoryResult.Success-> RepositoryResult.Success("Query Berhasil Didapat")
            is RepositoryResult.Error-> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled-> RepositoryResult.Canceled(query.exception)
        }
    }
}