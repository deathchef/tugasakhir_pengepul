package com.example.pengepul.repo

import android.net.Uri

interface StorageRongsokRepo {
    suspend fun downloadFotoRongsok(uid: String, nama_barang: String): Uri
    suspend fun uploadFotoRongsok(
        uid: String,
        nama_barang: String,
        foto_rongsok: Uri,
        onResult: (Uri) -> Unit
    )
}