package com.example.pengepul.repo

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.example.pengepul.model.RepositoryResult

interface  StorageRepo {
    suspend fun downloadFotoToko(uid: String, nama_toko: String): Uri
    suspend fun uploadFotoToko(uid: String, nama_toko: String, foto_toko: Uri,  onResult: (Uri) -> Unit)
}