package com.example.pengepul.repo

import com.example.pengepul.model.Pengepul
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.ui.mainmenu.toko.Toko

interface TokoRepo {
    suspend fun getDataToko(uid: String): RepositoryResult<Toko>
    suspend fun addDataToko(
        nama: String,
        latitude: Double,
        longitude: Double,
        address: String,
        tokenFcm: String,
        noTelp: String
    ): RepositoryResult<String>

    suspend fun getDataPengepul(): RepositoryResult<Pengepul>
    suspend fun editDataToko(
        nama: String,
        latitude: Double,
        longitude: Double,
        address: String
    ): RepositoryResult<String>
//    suspend fun addAlamat(
//        alamat: Alamat
//    ): RepositoryResult<String>
//    suspend fun getAlamat(uid: String): RepositoryResult<Alamat>
}