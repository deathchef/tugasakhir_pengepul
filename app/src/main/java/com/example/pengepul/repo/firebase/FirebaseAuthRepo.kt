package com.example.pengepul.repo.firebase

import android.util.Log
import com.example.pengepul.model.Pengepul
import com.example.pengepul.model.RepositoryResult
import com.example.pengepul.repo.AuthFirebaseRepo
import com.example.pengepul.utils.Constants
import com.example.pengepul.utils.FirebaseTaskExtention.awaits
import com.example.pengepul.utils.firestore
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.SetOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.tasks.await

class FirebaseAuthRepo(private val auth: FirebaseAuth) : AuthFirebaseRepo {
    override suspend fun login(email: String, password: String): RepositoryResult<String> {
        return when (val query = auth.signInWithEmailAndPassword(email, password).awaits()) {
            is RepositoryResult.Success -> RepositoryResult.Success("Login Berhasil")
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
    }

    override suspend fun register(nama: String, email: String, password: String, phone: String, tokenFCM: String): RepositoryResult<String> {
        when (val query = auth.createUserWithEmailAndPassword(email, password).awaits()) {
            is RepositoryResult.Error -> RepositoryResult.Error(query.exception)
            is RepositoryResult.Canceled -> query.exception.let {
                RepositoryResult.Canceled(it)
            }
        }

        val uid = auth.currentUser?.uid.toString()

        val pengepul = Pengepul(
            id = uid,
            name = nama,
            email = email,
            password = password,
            noHp = phone,
            tokenFcm = tokenFCM,
            isBanned = false
//            id_toko =
        )
        val database = Firebase.database
        val ref = database.getReference("/users/$uid")
        return when (val db =
            firestore.collection(Constants.PENGEPUL).document(uid).set(pengepul, SetOptions.merge()).awaits()) {
            is RepositoryResult.Success -> {
                auth.currentUser?.let { it ->
                    val profilUpdate = UserProfileChangeRequest.Builder()
                        .setDisplayName(nama)
                        .build()
                    it.updateProfile(profilUpdate)
                }
                ref.setValue(pengepul).addOnSuccessListener {
                    Log.d("Database","Success Save data di database")
                }
                RepositoryResult.Success("Register Berhasil")
            }
            is RepositoryResult.Error -> RepositoryResult.Error(db.exception)
            is RepositoryResult.Canceled -> db.exception.let {
                RepositoryResult.Canceled(it)
            }
        }
    }
}