package com.example.pengepul.utils

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage

import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class Apps: Application(){
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        startKoin {
            androidContext(this@Apps)
            modules(module {
                single { FirebaseAuth.getInstance() }
                single { Firebase.storage.reference }
                single { DefaultDispatchersProvider() as DispatcherProvider }
            })
            modules(koinModule)
            modules(firebaseRepo)
            modules(viewModel)
        }
    }
}
val firestore = Firebase.firestore