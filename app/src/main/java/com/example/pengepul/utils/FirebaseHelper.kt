package com.example.pengepul.utils

import com.google.firebase.auth.FirebaseAuth

class FirebaseHelper(private val auth: FirebaseAuth) {
    fun firebaseLogin(): Boolean{
        return auth.currentUser != null
    }

    fun firebaseSignOut(){
        auth.signOut()
    }
}