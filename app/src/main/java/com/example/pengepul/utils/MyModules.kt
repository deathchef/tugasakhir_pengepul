package com.example.pengepul.utils

import com.example.pengepul.repo.firebase.*
import com.example.pengepul.ui.login.LoginVM
import com.example.pengepul.ui.mainmenu.profil.ProfilVM
import com.example.pengepul.ui.mainmenu.rongsok.RongsokVM
import com.example.pengepul.ui.mainmenu.toko.TokoVM
import com.example.pengepul.ui.mainmenu.transaksi.TransaksiVM
import com.example.pengepul.ui.notifications.NotificationViewModel
import com.example.pengepul.ui.notifications.detailtransaksi.DetailTransaksiViewModel
import com.example.pengepul.ui.register.RegisVM
import org.koin.dsl.module

var koinModule = module {
    factory { FirebaseHelper(get()) }
}

val firebaseRepo = module {
    factory { FirebaseUserRepo(get()) }
    factory { FirebaseAuthRepo(get()) }
    factory { FirebaseTokoRepo(get())}
    factory { FirebaseStorageRepo(get()) }
    factory { FirebaseStorageRongsok(get()) }
    factory { FirebaseRongsokRepo(get()) }
    factory { FirebaseNotificationRepo() }
    factory { FirebaseTransaksiRepo(get(),get()) }
}

val viewModel = module {
    factory { LoginVM.Factory(get(), get()) }
    factory { RegisVM.Factory(get(), get()) }
    factory { TokoVM.Factory(get(),get(),get(),get()) }
    factory { RongsokVM.Factory(get(), get(), get(), get(), get()) }
    factory { ProfilVM.Factory(get(),get()) }
    factory { NotificationViewModel.Factory(get(),get(),get()) }
    factory { TransaksiVM.Factory(get(),get(),get()) }
    factory { DetailTransaksiViewModel.Factory(get(), get(), get()) }
}
