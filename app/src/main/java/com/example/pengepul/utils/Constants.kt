package com.example.pengepul.utils

object Constants {

    const val PENGEPUL: String = "pengepul"
    const val TOKO: String = "toko"
    const val USER: String = "users"
    const val RONGSOK: String = "rongsok"
    const val TRANSAKSI: String = "transaksi"
    const val JENIS: String = "jenis_rongsok"
    const val SERVER_KEY: String =
        "key=AAAA46B_VmQ:APA91bFQkDwzsgDvZxL0201-zrpMmbAs0hjHEDIrypoXhiRHHE0MmAfPRu60qYXtJHuwfGE4Y3bAI8_kwuhSOx40_J3terfVrKqafs3JnNOS924wicDu2_5auR9bAceMZGhuCWwiZAME"
    const val CONTENT_TYPE = "application/json;charset=UTF-8"
    const val BASE_URL_NOTIF = "https://fcm.googleapis.com/"
    const val SEND_NOTIF = "fcm/send"
    const val NOTIFICATIONS = "notifications"
    const val TITLE = "title"
    const val MESSAGE = "message"
    const val TYPE = "type"
    const val SHOP = "Shop"
    const val CHAT = "Chat"
    const val RONGSOK_TRANSAKSI = "rongsok_transaksi"
}