package com.example.pengepul.services

import com.example.pengepul.BuildConfig
import com.example.pengepul.model.BodyPayloadNotif
import com.example.pengepul.utils.Constants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object DataManager {
    private fun serviceNotif(): ApiServices{
        val client = OkHttpClient().newBuilder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG)
                    HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            })
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        val retrofit  = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL_NOTIF)
            .client(client)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(ApiServices::class.java)
    }

    fun pushNotif(bodyPayloadNotif: BodyPayloadNotif) = serviceNotif()
        .pushNotif(Constants.SERVER_KEY, bodyPayloadNotif)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}