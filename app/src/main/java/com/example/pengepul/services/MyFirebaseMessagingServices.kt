package com.example.pengepul.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.pengepul.R
import com.example.pengepul.model.BodyPayloadNotif
import com.example.pengepul.ui.notifications.NotificationsActivity
import com.example.pengepul.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import io.karn.notify.Notify
import org.json.JSONObject
import kotlin.random.Random

class MyFirebaseMessagingServices : FirebaseMessagingService() {
    private val TAG = MyFirebaseMessagingServices::class.java.simpleName

    lateinit var manager: NotificationManager
    var auth = FirebaseAuth.getInstance()
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        var id = auth.currentUser?.uid
        Log.d(TAG, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        remoteMessage.data.isNotEmpty().let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
            val jsonData = Gson().toJson(remoteMessage.data)

            sendBroadcast(Intent(this, BroadcastReceiver::class.java))

            if (jsonData!= null){
                val jsonObject = JSONObject(jsonData)
                val type = jsonObject.getString("type")
                when (type){
                    Constants.SHOP ->{
                        val model = Gson().fromJson(jsonData, BodyPayloadNotif.Data::class.java)
                        if (id == model.id_Pengepul){
                            sendNotification(model.tittle.toString(), model.message.toString())
                        }
                    }
                }
            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            sendNotification(
                messageTitle = it.title.toString(),
                messageBody = it.body.toString()
            )
            Log.d(TAG, "Message Notification: ${it.title}, body: ${it.body}")
        }
    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        sendRegistrationToServer(token)
    }

    private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageTitle: String, messageBody: String) {
        val CHANNEL_ID = getString(R.string.default_notification_channel_id)
        val intent = Intent(applicationContext, NotificationsActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var channel = NotificationChannel(
                CHANNEL_ID,
                "pushNotification",
                NotificationManager.IMPORTANCE_HIGH
            )
            manager.createNotificationChannel(channel)
        }

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setSmallIcon(R.drawable.logo)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)

        var pendingIntent =
            PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        builder.setContentIntent(pendingIntent)
        manager.notify(0, builder.build())
    }
}