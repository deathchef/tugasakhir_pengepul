package com.example.pengepul.services

import com.example.pengepul.model.BodyPayloadNotif
import com.example.pengepul.utils.Constants
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiServices {
    @POST(Constants.SEND_NOTIF)
    @Headers("Content-Type: ${Constants.CONTENT_TYPE}")
    fun pushNotif(
        @Header("Authorization") token: String,
        @Body bodyPayLoadNotif: BodyPayloadNotif
    ): Single<String>
}